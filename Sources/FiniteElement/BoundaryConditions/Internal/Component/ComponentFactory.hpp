///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 15:19:05 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HPP_

# include <functional>
# include <map>

# include "Utilities/Singleton/Singleton.hpp"
# include "Utilities/Exceptions/Factory.hpp"

# include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp"



namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {



            /*!
             * \brief The purpose of this class is to create on demand a pointer to a newly created object
             * which type depends on the name given in a specific format.
             *
             *
             *
             */
            class ComponentFactory final : public Utilities::Singleton<ComponentFactory>
            {
            public:


                //! Alias for a function which will create a 'Component'
                using FunctionPrototype = std::function<ComponentManager::const_shared_ptr()>;

                /*!
                 * \brief Alias for call back.
                 *
                 * \internal <b><tt>[internal]</tt></b> As very few variables are expected, a std::map is used rather than a std::unordered_map.
                 * This choice might be questioned: I read once that for more than 3 keys the has map is alreadyy more efficient...
                 */
                using CallBack = std::map<std::string, FunctionPrototype>;

                //! Name of the class (required for some Singleton-related errors).
                static const std::string& ClassName();



            public:


                /*!
                 * \brief Register a variable.
                 *
                 */
                template<class VariableT>
                bool Register(FunctionPrototype function);

                /*!
                 * \brief Create an object according to its name.
                 */
                ComponentManager::const_shared_ptr CreateFromName(const std::string& component_name) const;

                //! Number of elements registered in the factory.
                inline CallBack::size_type Nvariable() const;



            private:


                //! \name Singleton requirements.
                ///@{
                //! Constructor.
                ComponentFactory();

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<ComponentFactory>;
                ///@}



            private:



                /*!
                 * \brief Associative container to choose the right function given its string identifier.
                 *
                 *
                 */
                CallBack callbacks_;


            };


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HPP_
