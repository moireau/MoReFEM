///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{
    
    
    const std::string& NumberingSubset::ClassName()
    {
        static std::string ret("NumberingSubset");
        return ret;
    }
    
    
    NumberingSubset::NumberingSubset(unsigned int id, bool do_move_mesh)
    : unique_id_parent(id),
    do_move_mesh_(do_move_mesh)
    { }
    
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
