///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HPP_
# define MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HPP_




namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Crtp
    {


        /// \addtogroup CoreGroup
        ///@{


        /*!
         * \brief This Crtp add two data attributes (const references to row and column numbering subsets) and accessors
         * to them.
         */
        template<class DerivedT>
        class NumberingSubsetForMatrix
        {

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit NumberingSubsetForMatrix(const NumberingSubset& row_numbering_subset,
                                              const NumberingSubset& col_numbering_subset);

            //! Destructor.
            ~NumberingSubsetForMatrix() = default;

            //! Copy constructor.
            NumberingSubsetForMatrix(const NumberingSubsetForMatrix&);

            //! Move constructor.
            NumberingSubsetForMatrix(NumberingSubsetForMatrix&&) = default;

            //! Copy affectation.
            NumberingSubsetForMatrix& operator=(const NumberingSubsetForMatrix&) = default;

            //! Move affectation.
            NumberingSubsetForMatrix& operator=(NumberingSubsetForMatrix&&) = default;

            ///@}


            //! Numbering subset used to describe rows.
            const NumberingSubset& GetRowNumberingSubset() const;

            //! Numbering subset used to describe columns.
            const NumberingSubset& GetColNumberingSubset() const;


        private:


            //! Numbering subset used to describe rows.
            const NumberingSubset& row_numbering_subset_;

            //! Numbering subset used to describe columns.
            const NumberingSubset& col_numbering_subset_;

        };


        ///@} // \addtogroup CoreGroup


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/Crtp/NumberingSubsetForMatrix.hxx"


#endif // MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HPP_
