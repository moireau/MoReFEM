///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HPP_

#include <algorithm>

# include "Utilities/MatrixOrVector.hpp"

# include "Core/NumberingSubset/NumberingSubset.hpp"

# include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Store elementary matrices of type \a MatrixType along with the numbering subset pair that can pinpoint
             * each of them.
             *
             */

            template<class DerivedT>
            class NumberingSubsetSubMatrix
            {

            public:

                //! Convenient alias.
                using list_type = SubMatrixForNumberingSubsetPair::vector_unique_ptr;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit NumberingSubsetSubMatrix() = default;

                //! Destructor.
                ~NumberingSubsetSubMatrix() = default;

                //! Copy constructor.
                NumberingSubsetSubMatrix(const NumberingSubsetSubMatrix&) = default;

                //! Move constructor.
                NumberingSubsetSubMatrix(NumberingSubsetSubMatrix&&) = default;

                //! Copy affectation.
                NumberingSubsetSubMatrix& operator=(const NumberingSubsetSubMatrix&) = default;

                //! Move affectation.
                NumberingSubsetSubMatrix& operator=(NumberingSubsetSubMatrix&&) = default;

                ///@}

                //! Get the local matrix that matches the given numbering subsets.
                LocalMatrix& GetSubMatrix(const NumberingSubset& row_numbering_subset,
                                          const NumberingSubset& col_numbering_subset) const;

                //! Allocate the sub-matrices.
                template<class ElementaryDataT>
                void AllocateSubMatrices(const ElementaryDataT& elementary_data);

            private:


                //! Accessor to the underlying list.
                const list_type& GetSubMatrixList() const;

            private:

                //! List of combination sub matrix/numbering subset pair.
                list_type sub_matrix_list_;

            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HPP_
