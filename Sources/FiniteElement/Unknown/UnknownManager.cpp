///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 09:05:36 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{
    
    
    const std::string& UnknownManager::ClassName()
    {
        static std::string ret("UnknownManager");
        return ret;
    }

    
    void UnknownManager::RegisterUnknown(const Unknown::const_shared_ptr& unknown_ptr)
    {
        assert(!(!unknown_ptr));
        
        const auto& unknown_list = GetList();
        const auto& name = unknown_ptr->GetName();

        if (std::find_if(unknown_list.cbegin(),
                         unknown_list.cend(),
                         [&name](const auto& unknown_in_list_ptr)
                         {
                             assert(!(!unknown_in_list_ptr));
                             return unknown_in_list_ptr->GetName() == name;
                         }) != unknown_list.cend())
            throw ExceptionNS::Dof::DuplicatedUnknownInInputFile(name, __FILE__, __LINE__);
        
        const auto& unique_id = unknown_ptr->GetUniqueId();
        
        if (std::find_if(unknown_list.cbegin(),
                         unknown_list.cend(),
                         [unique_id](const auto& unknown_in_list_ptr)
                         {
                             assert(!(!unknown_in_list_ptr));
                             return unknown_in_list_ptr->GetUniqueId() == unique_id;
                         }) != unknown_list.cend())
            throw Exception("Two different unknowns can't share the same unique id (namely "
                            + std::to_string(unique_id) + ")", __FILE__, __LINE__);
        
        unknown_list_.push_back(unknown_ptr);
    }
    
    
    Unknown::const_shared_ptr UnknownManager::GetUnknownPtr(unsigned int unique_id) const
    {
        const auto& unknown_list = GetList();
        
        auto it = std::find_if(unknown_list.cbegin(), unknown_list.cend(),
                               [unique_id](const Unknown::const_shared_ptr& unknown_ptr)
                               {
                                   assert(!(!unknown_ptr));
                                   return unknown_ptr->GetUniqueId() == unique_id;
                               });
        
        assert(it != unknown_list.cend());
        return *it;
    }
    

    Unknown::const_shared_ptr UnknownManager::GetUnknownPtr(const std::string& unknown_name) const
    {
        const auto& unknown_list = GetList();
        
        auto it = std::find_if(unknown_list.cbegin(), unknown_list.cend(),
                               [&unknown_name](const Unknown::const_shared_ptr& unknown_ptr)
                               {
                                   assert(!(!unknown_ptr));
                                   return unknown_ptr->GetName() == unknown_name;
                               });
        
        assert(it != unknown_list.cend() && "Make sure the unknown has been properly registered. The registration "
               "is to be performed by your Model; make sure the unknown is correctly there in its UnknownTuple.");
        assert(!(!(*it)));
        return *it;
    }
    
    
    void UnknownManager::Create(unsigned int unique_id,
                                const std::string& name,
                                const std::string& str_nature)
    {
        UnknownNS::Nature nature;
        
        if (str_nature == "scalar")
            nature = UnknownNS::Nature::scalar;
        else if (str_nature == "vectorial")
            nature = UnknownNS::Nature::vectorial;
        else
        {
            nature = UnknownNS::Nature::vectorial; // to avoid release mode warning.
            assert(false);
        }
        
        auto raw_ptr = new Unknown(name, unique_id, nature);
        
        auto unknown_ptr = Unknown::const_shared_ptr(raw_ptr);
        
        RegisterUnknown(unknown_ptr);
    }
    
    
    void WriteUnknownList(const std::string& output_directory)
    {
        std::ostringstream oconv;
        oconv << output_directory << "/unknowns.hhdata";
        std::ofstream out;
        FilesystemNS::File::Create(out, oconv.str(), __FILE__, __LINE__);
        
        const auto& storage = UnknownManager::GetInstance().GetList();
        
        for (const auto& unknown_ptr : storage)
        {
            assert(!(!unknown_ptr));
            const auto& unknown = *unknown_ptr;
            
            out << unknown.GetName() << " : " << unknown.GetNature() << std::endl;
        }
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
