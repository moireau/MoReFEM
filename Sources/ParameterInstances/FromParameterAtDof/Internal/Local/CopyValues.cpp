///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#include "ParameterInstances/FromParameterAtDof/Internal/Local/CopyValues.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace LocalParameterOperatorNS
        {
            
            
            void CopyValue(double source, double& target)
            {
                target = source;
            }
            
            
            void CopyValue(const LocalVector& source, LocalVector& target)
            {
                assert(source.GetSize() == target.GetSize());
                const auto size = source.GetSize();
                
                for (auto i = 0; i < size; ++i)
                    target(i) = source(i);
            }
            
            
            void CopyValue(const LocalMatrix& source, LocalMatrix& target)
            {
                assert(source.GetM() == target.GetM());
                assert(source.GetN() == target.GetN());
                const auto Nrow = source.GetM();
                const auto Ncol = source.GetN();
                
                for (auto i = 0; i < Nrow; ++i)
                    for (auto j = 0; j < Ncol; ++j)
                        target(i, j) = source(i, j);
            }

            
        } // namespace LocalParameterOperatorNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
