///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:56:52 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_

# include <memory>
# include <vector>
# include <array>
# include <cassert>

# include "Utilities/Pragma/Pragma.hpp"

# include "Core/Enum.hpp"

# include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
# include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"
# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
# include "FiniteElement/Unknown/Unknown.hpp"
# include "FiniteElement/QuadratureRules/QuadratureRule.hpp"

# include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {




            /*!
             * \brief Defines all of ElementaryData that does not depend upon the kind of object considered (matrix or vector).
             */
            class ElementaryDataImpl
            {


            public:

                /// \name Special members.

                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
                 * list of unknowns involved for the operator related to this ElementaryData will provide all the data
                 * required to build RefFEltInLocalOperator objects that allow easy navigation through the
                 * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the required
                 * data).
                 * \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
                 * \param[in] unknown_storage List of unknowns involved in the current local operator. This must be a subset
                 * of the ones in the enclosing FEltSpace; however there is no need to follow the same ordering (ordering
                 * is specified at the LocalVariationalOperator level).
                 * \param[in] test_unknown_storage List of all the pair test_unknown/numbering subset considered by the operator.
                 * \param[in] felt_space_dimension Dimension considered in the finite element space.
                 * \param[in] geometric_mesh_region_dimension Dimension in the geometric mesh region.
                 *  Must be equal or higher than felt_space_dimension.
                 * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
                 */
                explicit ElementaryDataImpl(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                            const QuadratureRule& quadrature_rule,
                                            const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                            const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                            unsigned int felt_space_dimension,
                                            unsigned int geometric_mesh_region_dimension,
                                            AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

            protected:

                //! Destructor.
                ~ElementaryDataImpl() = default;

            public:

                //! Copy constructor.
                ElementaryDataImpl(const ElementaryDataImpl&) = delete;

                //! Move constructor.
                ElementaryDataImpl(ElementaryDataImpl&&) = default;

                //! Copy affectation.
                ElementaryDataImpl& operator=(const ElementaryDataImpl&) = delete;

                //! Move affectation.
                ElementaryDataImpl& operator=(ElementaryDataImpl&&) = default;
                ///@}

            public:

                //! Computes internal quantities related to finite element. Must absolutely be called before each finite
                // element is used; will disappear shortly as it is way too dangerous.
                void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);




                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of nodes.
                 */
                unsigned int NnodeRow() const noexcept;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of nodes.
                 */
                unsigned int NnodeCol() const noexcept;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of dofs.
                 */
                unsigned int NdofRow() const noexcept;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of dofs.
                 */
                unsigned int NdofCol() const noexcept;

                //! Dimension of the \a GeometricElt.
                unsigned int GetGeomEltDimension() const noexcept;

                //! Number of quadrature points.
                unsigned int NquadraturePoint() const noexcept;

                //! Access the properties shared by all the FElts related to the current local variational operator.
                const Advanced::RefFEltInLocalOperator& GetRefFElt(const ExtendedUnknown& unknown) const;

                //! Access the properties shared by all the FElts related to the current local variational operator.
                const Advanced::RefFEltInLocalOperator& GetTestRefFElt(const ExtendedUnknown& unknown) const;

                //! Number of \a Unknown.
                unsigned int Nunknown() const noexcept;

                //! Number of \a Unknown.
                unsigned int NtestUnknown() const noexcept;

                //! Get the dimension of the finite element space.
                unsigned int GetFEltSpaceDimension() const noexcept;

                //! Get the dimension of the geometric mesh region.
                unsigned int GetMeshDimension() const noexcept;

                //! Accessor to informations at each quadrature point (vector index stands for a quadrature point index).
                const std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& GetInformationsAtQuadraturePointListForUnknown() const noexcept;

                //! Accessor to informations at each quadrature point (vector index stands for a quadrature point index).
                const std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& GetInformationsAtQuadraturePointListForTestUnknown() const noexcept;

                //! Accessor to elements of infos_at_quad_pt_list.
                const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint&
                    GetInformationsAtQuadraturePointForUnknown(unsigned int quadrature_pt_index) const noexcept;

                //! Accessor to elements of infos_at_quad_pt_list.
                const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint&
                GetInformationsAtQuadraturePointForTestUnknown(unsigned int quadrature_pt_index) const noexcept;

                //! Access to the current \a LocalFEltSpace considered.
                const LocalFEltSpace& GetCurrentLocalFEltSpace() const noexcept;

                //! Access to the current geometric element considered.
                const GeometricElt& GetCurrentGeomElt() const noexcept;

                //! Returns the quadrature rule to use.
                const QuadratureRule& GetQuadratureRule() const noexcept;


            protected:


                //! \name Access to cached geometric data.
                ///@{

                //! Number of \a LocalNode in current \a RefGeomElt.
                unsigned int NnodeInRefGeomElt() const noexcept;

                ///@}

            private:

                /*!
                 * \brief Get the underlying \a RefGeomElt.
                 */
                const RefGeomElt& GetRefGeomElt() const  noexcept;

                /*!
                 * \brief Access the \a RefLocalFEltSpace.
                 *
                 * \internal <b><tt>[internal]</tt></b> This accessor is public but should only be used by
                 * LocalVariationalOperators.
                 *
                 * \return Underlying \a RefLocalFEltSpace.
                 */
                const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace() const noexcept;


            private:

                //! Fill the point_ matrix with the coordinates of the points of the geometric element in the mesh.
                void UpdateCoordinates(const GeometricElt& geometric_element);

                //! Set the number of components and the related sequence.
                void SetGeomEltDimension();

            private:


                //! Non constant accessor to informations at each quadrature point (vector index stands for a quadrature point index).
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& GetNonCstInformationsAtQuadraturePointListForUnknown();

                //! Non constant accessor to informations at each quadrature point (vector index stands for a quadrature point index).
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& GetNonCstInformationsAtQuadraturePointListForTestUnknown();

                /*!
                 * \brief Init the reference finite elements.
                 *
                 * Numbering subset are completely dropped in this method: they are irrelevant at a local level,
                 * which is where ElementaryData is used.
                 *
                 * \param[in] extended_unknown_list List of \a ExtendedUnknown considered in the current object.
                 */
                void InitReferenceFiniteElements(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list);

                /*!
                 * \brief Init the reference finite elements.
                 *
                 * Numbering subset are completely dropped in this method: they are irrelevant at a local level,
                 * which is where ElementaryData is used.
                 *
                 * \param[in] extended_test_unknown_list List of \a ExtendedUnknown considered in the current object.
                 */
                void InitTestReferenceFiniteElements(const ExtendedUnknown::vector_const_shared_ptr& extended_test_unknown_list);

                //! Access the list of reference finite elements.
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& GetRefFEltList() const noexcept;

                //! Access the list of reference finite elements.
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& GetTestRefFEltList() const noexcept;

                //! Set the pointer to \a LocalFEltSpace under consideration.
                void SetCurrentLocalFEltSpace(const LocalFEltSpace* local_felt_space);


            private:

                //! Finite element type for which the elementary data are produced.
                const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space_;

                //! Quadrature rule to use.
                const QuadratureRule& quadrature_rule_;

                //! Coordinates of the points defining the element: point_(ipoint,jcoor)  (in the current FE)
                LocalMatrix point_;

                /*!
                 * \brief Stores the informations at quadrature point level.
                 *
                 * The index of the vector stands for a quadrature point.
                 */
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint> infos_at_quad_pt_list_for_unknown_;

                /*!
                 * \brief Stores the informations at quadrature point level.
                 *
                 * The index of the vector stands for a quadrature point.
                 */
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint> infos_at_quad_pt_list_for_test_unknown_;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                unsigned int Nnode_row_;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                unsigned int Nnode_col_;


                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                unsigned int Ndof_row_;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                unsigned int Ndof_col_;


                //! Dimension considered in the finite element space.
                const unsigned int felt_space_dimension_;

                //! Dimension in the geometric mesh region. Must be equal or higher than felt_space_dimension_.
                const unsigned int geometric_mesh_region_dimension_;

                /*!
                 * \brief LocalFEltSpace under consideration.
                 *
                 * \warning Do not delete this pointer!
                 *
                 */
                const LocalFEltSpace* current_local_felt_space_ = nullptr;


            private:


                //! \name Cached geometric data.
                ///@{

                unsigned int geom_elt_dimension_ = NumericNS::UninitializedIndex<unsigned int>();


                /*!
                 * \brief Number of Coords object required to describe fully a GeometricElt of this type.
                 *
                 * For instance 27 for an Hexahedron 27 or 1 for a Point.
                 */
                unsigned int Ncoords_in_geom_ref_elt_  = NumericNS::UninitializedIndex<unsigned int>();

                ///@}


            private:

                //! List of reference finite elements.
                Advanced::RefFEltInLocalOperator::vector_const_unique_ptr ref_felt_list_;

                //! List of reference finite elements.
                Advanced::RefFEltInLocalOperator::vector_const_unique_ptr test_ref_felt_list_;

            };



        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_
