///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace Source
        {


            template<unsigned int IndexT>
            const std::string& PressureFromFile<IndexT>::GetName()
            {
                static std::string ret = Impl::GenerateSectionName("PressureFromFile", IndexT);
                return ret;
            }


            template<unsigned int IndexT>
            const std::string& PressureFromFile<IndexT>::FilePath::NameInFile()
            {
                static std::string ret("FilePath");
                return ret;
            }


            template<unsigned int IndexT>
            const std::string& PressureFromFile<IndexT>::FilePath::Description()
            {
                static std::string ret("Path of the file to use. "
                                       "Format: "
                                       "time pressure "
                                       "value1 value1 "
                                       "value2 value2 "
                                       "... ");
                return ret;
            }


            template<unsigned int IndexT>
            const std::string& PressureFromFile<IndexT>::FilePath::Constraint()
            {
                return Utilities::EmptyString();
            }


            template<unsigned int IndexT>
            const std::string& PressureFromFile<IndexT>::FilePath::DefaultValue()
            {
                return Utilities::EmptyString();
            }


        } // namespace Source


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_
