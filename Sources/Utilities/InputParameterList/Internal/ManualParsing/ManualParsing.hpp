///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_MANUAL_PARSING_x_MANUAL_PARSING_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_MANUAL_PARSING_x_MANUAL_PARSING_HPP_

# include <unordered_map>
# include <vector>
# include <string>

# include "Utilities/InputParameterList/Definitions.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            //! Convenient alias.
            using EntriesSortPerSection = std::unordered_map<std::string, std::vector<std::string> >;


            /*!
             * \brief Parse the file to extract all the keys in it.
             *
             * \internal <b><tt>[internal]</tt></b> This code is temporary and should be replaced in due time by proper Lua code, but as I am
             * not fluent in Lua I am not able to fulfill two requirements for InputParameterList:
             * - Forbid duplicates in the  keys of the input parameter file. If left to Lua, when one variable
             * is assigned twice second value is considered.
             * - Indicate the keys that are useless.
             * This function provides the list of all the keys, and therefore allows to make these checks. However,
             * the parsing is not completely bullet-proof and the formatting is more constrained than what is
             * actually required by Lua.
             *
             * \param[in] path Path to the input Lua file.
             * \return The keys sort by section. Key of EntriesSortPerSection is the section name, and the values are
             * stored in the second term.
             */
            EntriesSortPerSection ExtractKeysFromFile(const std::string& path);



        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_MANUAL_PARSING_x_MANUAL_PARSING_HPP_
