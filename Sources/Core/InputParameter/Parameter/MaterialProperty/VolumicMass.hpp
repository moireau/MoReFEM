///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 12:18:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"
# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace MaterialProperty
        {


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            template<class EnclosingTypeT>
            struct VolumicMass  : public Crtp::Section<VolumicMass<EnclosingTypeT>, EnclosingTypeT>
            {


                //! Convenient alias.
                using self = VolumicMass;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, EnclosingTypeT>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines Poisson coefficient value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct VolumicMass


        } // namespace MaterialProperty


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/MaterialProperty/VolumicMass.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_
