/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 13 Sep 2017 22:46:51 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Filesystem/Folder.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Operators/TestFunctions/VariationalFormulation.hpp"


namespace MoReFEM
{
    
    
    namespace TestFunctionsNS
    {


        VariationalFormulation::VariationalFormulation(const morefem_data_type& morefem_data,
                                                       TimeManager& time_manager,
                                                       const GodOfDof& god_of_dof,
                                                       DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list))
        { }
      

        void VariationalFormulation::SupplInit(const InputParameterList& input_parameter_data)
        {
            static_cast<void>(input_parameter_data);
            
            auto& fibers_in_volume = Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance().GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fibers_in_volume));
            
            fibers_in_volume.Initialize();
            
            auto& fibers_on_surface = Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance().GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fibers_on_surface));
            
            fibers_on_surface.Initialize();
            
            auto& angles = Internal::FiberNS::FiberListManager<ParameterNS::Type::scalar>::GetInstance().GetNonCstFiberList(EnumUnderlyingType(FiberIndex::angles));
            
            angles.Initialize();
            
            using parameter_type = Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::scalar,
                ::MoReFEM::ParameterNS::Policy::Constant,
                ParameterNS::TimeDependencyNS::None
            >;
            
            decltype(auto) domain_manager = DomainManager::GetInstance();
            
            decltype(auto) domain_volume =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);
            decltype(auto) domain_full_mesh =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            decltype(auto) domain_surface =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::surface), __FILE__, __LINE__);
            
            intracellular_trans_diffusion_tensor_ = std::make_unique<parameter_type>("Intracellular Trans Diffusion tensor",
                                                                                     domain_full_mesh,
                                                                                     1.);
            
            extracellular_trans_diffusion_tensor_ = std::make_unique<parameter_type>("Extracellular Trans Diffusion tensor",
                                                                                     domain_full_mesh,
                                                                                     1.);
            
            intracellular_fiber_diffusion_tensor_ = std::make_unique<parameter_type>("Intracellular Fiber Diffusion tensor",
                                                                                     domain_full_mesh,
                                                                                     2.);
            
            extracellular_fiber_diffusion_tensor_ = std::make_unique<parameter_type>("Extracellular Fiber Diffusion tensor",
                                                                                     domain_full_mesh,
                                                                                     2.);
            
            heterogeneous_conductivity_coefficient_ = std::make_unique<parameter_type>("Heterogeneous conductivity coefficient",
                                                                                       domain_surface,
                                                                                       1.);
            
            density_ = std::make_unique<parameter_type>("Density",
                                                        domain_volume,
                                                        1.);
            
            young_modulus_ = InitScalarParameterFromInputData<InputParameter::Solid::YoungModulus>("Young modulus",
                                                                                                   domain_volume,
                                                                                                   input_parameter_data);
            
            poisson_ratio_ = InitScalarParameterFromInputData<InputParameter::Solid::PoissonRatio>("Poisson ratio",
                                                                                                   domain_volume,
                                                                                                   input_parameter_data);
            
            fluid_viscosity_ = InitScalarParameterFromInputData<InputParameter::Fluid::Viscosity>("Viscosity",
                                                                                                  domain_volume,
                                                                                                  input_parameter_data);
            
            static_pressure_ =std::make_unique<parameter_type>("StaticPressure",
                                                               domain_surface,
                                                               1.);
            
            using source_parameter_type = InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::source)>;
            
            source_parameter_ =
            InitScalarParameterFromInputData<source_parameter_type>("Volumic source",
                                                                    domain_volume,
                                                                    input_parameter_data);
            
            DefineOperators(input_parameter_data);
            AssembleStaticOperators();
        }
        
        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& god_of_dof = GetGodOfDof();
            
            const auto& potential_1_potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2));
            const auto& potential_1_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1));
            const auto& potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_2));
            const auto& potential_3_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_3));
            const auto& displacement_potential_1_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1));
            const auto& potential_1_potential_2_potential_4_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4));
            const auto& displacement_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement));
            const auto& P1_potential_1_P2_potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2));
            
            parent::AllocateSystemMatrix(potential_1_potential_2_numbering_subset, potential_1_potential_2_numbering_subset);
            parent::AllocateSystemVector(potential_1_potential_2_numbering_subset);
            
            parent::AllocateSystemMatrix(potential_1_numbering_subset, potential_1_numbering_subset);
            parent::AllocateSystemVector(potential_1_numbering_subset);
            
            parent::AllocateSystemMatrix(potential_2_numbering_subset, potential_2_numbering_subset);
                        
            parent::AllocateSystemMatrix(potential_3_numbering_subset, potential_3_numbering_subset);
            parent::AllocateSystemVector(potential_3_numbering_subset);
            
            parent::AllocateSystemMatrix(potential_1_numbering_subset, potential_3_numbering_subset);
            
            parent::AllocateSystemMatrix(displacement_potential_1_numbering_subset, displacement_potential_1_numbering_subset);
            
            parent::AllocateSystemMatrix(potential_1_potential_2_potential_4_numbering_subset, potential_1_potential_2_potential_4_numbering_subset);
            
            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);
            
            parent::AllocateSystemMatrix(P1_potential_1_P2_potential_2_numbering_subset, P1_potential_1_P2_potential_2_numbering_subset);
            
            matrix_bidomain_ = std::make_unique<GlobalMatrix>(GetSystemMatrix(potential_1_potential_2_numbering_subset, potential_1_potential_2_numbering_subset));
            
            matrix_grad_phi_tau_tau_grad_phi_ = std::make_unique<GlobalMatrix>(GetSystemMatrix(potential_1_numbering_subset, potential_1_numbering_subset));
        }
        
        
        void VariationalFormulation::DefineOperators(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();

            const auto& mesh = god_of_dof.GetGeometricMeshRegion();
            
            const auto dimension = mesh.GetDimension();
            decltype(auto) unknown_manager = UnknownManager::GetInstance();
            
            const auto& felt_space_volume_potential_1_potential_2 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2));
            const auto& felt_space_volume_potential_1 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1));
            const auto& felt_space_volume_potential_2 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_2));
            const auto& felt_space_volume_potential_3 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_3));
            const auto& felt_space_volume_displacement_potential_1 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1));
            const auto& felt_space_volume_potential_1_potential_2_potential_4 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2_potential_4));
            const auto& felt_space_volume_displacement = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_displacement));
            const auto& felt_space_surface_displacement = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_displacement));
            const auto& felt_space_surface_potential_1 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_potential_1));
            const auto& felt_space_surface_potential_1_potential_2 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_potential_1_potential_2));
            
            const auto& potential_1_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_1));
            const auto& potential_2_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_2));
            const auto& potential_3_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_3));
            //const auto& potential_4_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_4));
            const auto& displacement_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            mass_operator_potential_1_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_1,
                                                                     potential_1_ptr,
                                                                     potential_1_ptr);
            
            mass_operator_potential_2_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_2,
                                                                     potential_2_ptr,
                                                                     potential_2_ptr);
            
            mass_operator_potential_1_potential_1_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_1_potential_2,
                                                                                 potential_1_ptr,
                                                                                 potential_1_ptr);
            
            mass_operator_potential_1_potential_2_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_1_potential_2,
                                                                                 potential_1_ptr,
                                                                                 potential_2_ptr);
            
            mass_operator_potential_2_potential_1_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_1_potential_2,
                                                                                 potential_2_ptr,
                                                                                 potential_1_ptr);
            
            mass_operator_potential_2_potential_2_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_1_potential_2,
                                                                                 potential_2_ptr,
                                                                                 potential_2_ptr);
            
            quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(10,
                                                                                                            3);
            
            mass_operator_potential_3_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_3,
                                                                     potential_3_ptr,
                                                                     potential_3_ptr,
                                                                     quadrature_rule_per_topology_for_operators_.get());
            
            mass_operator_potential_1_potential_3_ = std::make_unique<GVO::Mass>(felt_space_volume_potential_3,
                                                                                 potential_1_ptr,
                                                                                 potential_3_ptr);

            stiffness_operator_potential_1_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_1,
                                                                                    potential_1_ptr,
                                                                                    potential_1_ptr);
            
            stiffness_operator_potential_1_potential_1_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_1_potential_2,
                                                                                                potential_1_ptr,
                                                                                                potential_1_ptr);
            
            stiffness_operator_potential_1_potential_2_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_1_potential_2,
                                                                                                potential_1_ptr,
                                                                                                potential_2_ptr);
            
            stiffness_operator_potential_2_potential_1_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_1_potential_2,
                                                                                                potential_2_ptr,
                                                                                                potential_1_ptr);
            
            stiffness_operator_potential_2_potential_2_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_1_potential_2,
                                                                                                potential_2_ptr,
                                                                                                potential_2_ptr);
            
            stiffness_operator_potential_3_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_3,
                                                                                    potential_3_ptr,
                                                                                    potential_3_ptr,
                                                                                    quadrature_rule_per_topology_for_operators_.get());
            
            stiffness_operator_potential_1_potential_3_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume_potential_3,
                                                                                                potential_1_ptr,
                                                                                                potential_3_ptr);
            

            if (source_parameter_ != nullptr)
            {
                source_operator_potential_1_ = std::make_unique<source_operator_type>(felt_space_volume_potential_1,
                                                                                      potential_1_ptr,
                                                                                      *source_parameter_);
                
                source_operator_potential_1_potential_1_ = std::make_unique<source_operator_type>(felt_space_volume_potential_1_potential_2,
                                                                                                  potential_1_ptr,
                                                                                                  *source_parameter_);
                
                source_operator_potential_1_potential_2_ = std::make_unique<source_operator_type>(felt_space_volume_potential_1_potential_2,
                                                                                                  potential_2_ptr,
                                                                                                  *source_parameter_);
                
                source_operator_potential_3_ = std::make_unique<source_operator_type>(felt_space_volume_potential_3,
                                                                                      potential_3_ptr,
                                                                                      *source_parameter_);
            }
            
            variable_mass_operator_potential_1_ = std::make_unique<variable_mass_type>(felt_space_volume_potential_1,
                                                                                       potential_1_ptr,
                                                                                       potential_1_ptr,
                                                                                       *density_);

            const auto& fibers_in_volume = Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance().GetFiberList(EnumUnderlyingType(FiberIndex::fibers_in_volume));
            const auto& fibers_on_surface = Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance().GetFiberList(EnumUnderlyingType(FiberIndex::fibers_on_surface));
            const auto& angles = Internal::FiberNS::FiberListManager<ParameterNS::Type::scalar>::GetInstance().GetFiberList(EnumUnderlyingType(FiberIndex::angles));
            
            const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
            const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
            const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
            const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();
            
            const std::array<Unknown::const_shared_ptr, 2> potential_1_potential_2 { { potential_1_ptr, potential_2_ptr } };
            
            bidomain_operator_ = std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2,
                                                                 potential_1_potential_2,
                                                                 potential_1_potential_2,
                                                                 intracellular_trans_diffusion_tensor,
                                                                 extracellular_trans_diffusion_tensor,
                                                                 intracellular_fiber_diffusion_tensor,
                                                                 extracellular_fiber_diffusion_tensor,
                                                                 fibers_in_volume);
                        
            bidomain_potential_124_operator_ = std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2_potential_4,
                                                                               potential_1_potential_2,
                                                                               potential_1_potential_2,
                                                                               intracellular_trans_diffusion_tensor,
                                                                               extracellular_trans_diffusion_tensor,
                                                                               intracellular_fiber_diffusion_tensor,
                                                                               extracellular_fiber_diffusion_tensor,
                                                                               fibers_in_volume);
            
            if (dimension == 3)
            {
                surfacic_bidomain_operator_ = std::make_unique<GVO::SurfacicBidomain>(felt_space_surface_potential_1_potential_2,
                                                                                      potential_1_potential_2,
                                                                                      potential_1_potential_2,
                                                                                      intracellular_trans_diffusion_tensor,
                                                                                      extracellular_trans_diffusion_tensor,
                                                                                      intracellular_fiber_diffusion_tensor,
                                                                                      extracellular_fiber_diffusion_tensor,
                                                                                      *heterogeneous_conductivity_coefficient_,
                                                                                      fibers_on_surface,
                                                                                      angles);
            }

            grad_phi_tau_tau_grad_phi_operator_ = std::make_unique<GVO::GradPhiTauTauGradPhi>(felt_space_volume_potential_1,
                                                                                              potential_1_ptr,
                                                                                              potential_1_ptr,
                                                                                              extracellular_trans_diffusion_tensor,
                                                                                              extracellular_fiber_diffusion_tensor,
                                                                                              fibers_in_volume);
            
            grad_phi_tau_ortho_tau_grad_phi_operator_ = std::make_unique<GVO::GradPhiTauOrthoTauGradPhi>(felt_space_surface_potential_1,
                                                                                                         potential_1_ptr,
                                                                                                         potential_1_ptr,
                                                                                                         extracellular_trans_diffusion_tensor,
                                                                                                         extracellular_fiber_diffusion_tensor,
                                                                                                         fibers_on_surface,
                                                                                                         angles);
            
            const std::array<Unknown::const_shared_ptr, 2> displacement_potential_1 { { displacement_ptr, potential_1_ptr } };
            
            scalar_div_vectorial_operator_ = std::make_unique<GVO::ScalarDivVectorial>(felt_space_volume_displacement_potential_1,
                                                                                       displacement_potential_1,
                                                                                       displacement_potential_1,
                                                                                       3.,
                                                                                       2.);
            
            const auto mesh_dimension = god_of_dof.GetGeometricMeshRegion().GetDimension();
            
            elasticity_operator_ =  std::make_unique<GVO::GradOnGradientBasedElasticityTensor>(felt_space_volume_displacement,
                                                                                               displacement_ptr,
                                                                                               displacement_ptr,
                                                                                               *young_modulus_,
                                                                                               *poisson_ratio_,
                                                                                               ParameterNS::ReadGradientBasedElasticityTensorConfigurationFromFile(mesh_dimension,
                                                                                                                                                                   input_parameter_data));
            
            stokes_operator_ = std::make_unique<GVO::Stokes>(felt_space_volume_displacement_potential_1,
                                                             displacement_potential_1,
                                                             displacement_potential_1,
                                                             *fluid_viscosity_);
            
            ale_operator_ = std::make_unique<GVO::Ale>(felt_space_volume_displacement,
                                                       displacement_ptr,
                                                       displacement_ptr,
                                                       *density_);
            
            if (dimension == 3)
            {
                following_pressure_operator_ = std::make_unique<GVO::FollowingPressure<>>(felt_space_surface_displacement,
                                                                                          displacement_ptr,
                                                                                          displacement_ptr,
                                                                                          *static_pressure_);
            }
            
            decltype(auto) domain_volume =
                DomainManager::GetInstance().GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);
            
            reaction_law_ = std::make_unique<reaction_law_type>(input_parameter_data,
                                                                domain_volume,
                                                                GetTimeManager(),
                                                                felt_space_volume_potential_1.GetQuadratureRulePerTopology());
            
            non_linear_source_operator_ = std::make_unique<non_linear_source_operator_type>(felt_space_volume_potential_1,
                                                                                            potential_1_ptr,
                                                                                            *reaction_law_);
            
            solid_ = std::make_unique<Solid>(input_parameter_data,
                                             domain_volume,
                                             felt_space_volume_displacement.GetQuadratureRulePerTopology(),
                                             1.e50); // Big tolerance to avoid CheckConsistancy issues.
            
            hyperelastic_law_parent::Create(*solid_);
            
            pk2_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_volume_displacement,
                                                                    displacement_ptr,
                                                                    displacement_ptr,
                                                                    *solid_,
                                                                    GetTimeManager(),
                                                                    hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                                    nullptr,
                                                                    nullptr);
        }
        
        
    } // namespace TestFunctionsNS


} // namespace MoReFEM
