///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 May 2016 14:12:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_

# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            class InformationsAtQuadraturePoint;


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace OperatorNS
        {


            /*!
             * \brief Compute the gradient matrix related to a given local displacement.
             *
             * \param[in] infos_at_quad_pt Object that holds useful informations for computations at quadrature point.
             * \attention This method clearly expects to work within an operator which acts only upon a solid displacement
             * unknown. If your operator actually consider others unknown, you should rather create another operator
             * (typically a \a GlobalParameterOperator) which acts only on solid displacement and then call the resulting
             * value in your own operator (see \a UpdateCauchyGreenTensor use for an illustration).
             *
             * \param[in] local_displacement Displacement at the dofs of the finite element under consideration.
             * \param[out] gradient_matrix A square matrix which dimension is dimension of the mesh.
             */
            void ComputeGradientDisplacementMatrix(const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                                                   const std::vector<double>& local_displacement,
                                                   LocalMatrix& gradient_matrix);


        } // namespace OperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_
