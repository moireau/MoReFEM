import os

def FindNegativeValueInFile(myfile):
    
    FILE = open(myfile)
    
    negative = []
    
    counter = 0
    
    for line in FILE:
        value = float(line)
        
        if value < 0.:
            negative.append(counter)
            
        counter += 1
        
    if negative:
        print("Negative values were found in file {0} at line(s) {1}".format(myfile, negative))
        
        
def FindNegativeValueInFolder(folder):
    
    filelist = os.listdir(folder)
    
    for myfile in filelist:
        if myfile.endswith("hhdata") and not myfile.startswith('dof_information'):
            FindNegativeValueInFile(myfile)
    
        
        
if __name__ == "__main__":
    FindNegativeValueInFolder(".")