///
////// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Thu, 12 May 2016 16:34:28 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hpp"


namespace MoReFEM
{
    
    
    namespace RefineMeshNS
    {

        
        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }
        

    } // namespace RefineMeshNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
