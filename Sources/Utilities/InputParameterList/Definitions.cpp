///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 11:52:00 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include "Utilities/InputParameterList/Definitions.hpp"
#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM
{
    
    
    namespace Utilities
    {
        
        
        namespace InputParameterListNS
        {
            
            
            const std::string& NoEnclosingSection::GetName()
            {
                return EmptyString();
            }
            
            
            const std::string& NoEnclosingSection::GetFullName()
            {
                return EmptyString();
            }
                
          
            
        } // namespace InputParameterListNS
        
        
    } // namespace Utilities
  

} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
