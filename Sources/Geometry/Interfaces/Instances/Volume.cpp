///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/Interfaces/Instances/Volume.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        
        /*!
         * \brief Add a new volume to a mesh, and increment accordingly Nvolume_per_mesh.
         *
         * \param[in] mesh_identifier Identifier of the mesh to which the Volume is added.
         * \param[in,out] Nvolume_per_mesh Number of volumes per mesh (represented by its identifier).
         *
         * \return Index of the newly added volume. This index is 0 for first volume, then 1, and so forth...
         */
        unsigned int AddVolume(unsigned int mesh_identifier,
                               std::map<unsigned int, unsigned int>& Nvolume_per_mesh);

        
    } // namespace anonymous
    
    
    
    std::map<unsigned int, unsigned int>& Volume::NvolumePerMesh()
    {
        static std::map<unsigned int, unsigned int> ret;
        return ret;
    }
    
    
    
    Volume::Volume(const GeometricElt::shared_ptr& geometric_elt)
    : Internal::InterfaceNS::TInterface<Volume, InterfaceNS::Nature::volume>(),
    geometric_elt_(geometric_elt)
    {
        assert(!(!geometric_elt));
        
        decltype(auto) mesh_id = geometric_elt->GetMeshIdentifier();
        SetIndex(AddVolume(mesh_id, NvolumePerMesh()));
    }
    
    
    Volume::~Volume() = default;
    
    
    const Coords::vector_raw_ptr& Volume::GetVertexCoordsList() const noexcept
    {
        assert(!geometric_elt_.expired() && "By design the geometric element should be born by the same processor "
               "as the volume interface!");
        GeometricElt::shared_ptr shared_ptr = geometric_elt_.lock();
        
        return shared_ptr->GetCoordsList();
    }
    
    
    
    namespace // anonymous
    {
        
        
        unsigned int AddVolume(unsigned int mesh_identifier,
                               std::map<unsigned int, unsigned int>& Nvolume_per_mesh)
        {
            auto it = Nvolume_per_mesh.find(mesh_identifier);
            
            if (it == Nvolume_per_mesh.cend())
            {
                Nvolume_per_mesh.insert({mesh_identifier, 1u});
                return 0u;
            }
            else
                return it->second++;
        }
        
        
    } // namespace anonymous
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
