///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 31 Mar 2014 16:36:13 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_EXCEPTIONS_x_COORDS_HPP_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_EXCEPTIONS_x_COORDS_HPP_


# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace CoordsNS
        {


            //! Generic class for Coords exceptions.
            class Coords : public MoReFEM::Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Coords(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Coords();

                //! Copy constructor.
                Coords(const Coords&) = default;

                //! Move constructor.
                Coords(Coords&&) = default;

                //! Copy affectation.
                Coords& operator=(const Coords&) = default;

                //! Move affectation.
                Coords& operator=(Coords&&) = default;

            };


            /*!
             * \brief Thrown when a same Coords object has been assigned two incompatible types.
             *
             * For instance when a same Coords is said to be a Vertex and an Edge.
             */
            class InconsistentType : public Coords
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] index Index of the Coords for which the issue arose.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InconsistentType(unsigned int index, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~InconsistentType();

                //! Copy constructor.
                InconsistentType(const InconsistentType&) = default;

                //! Move constructor.
                InconsistentType(InconsistentType&&) = default;

                //! Copy affectation.
                InconsistentType& operator=(const InconsistentType&) = default;

                //! Move affectation.
                InconsistentType& operator=(InconsistentType&&) = default;
            };


        } // namespace CoordsNS


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_EXCEPTIONS_x_COORDS_HPP_
