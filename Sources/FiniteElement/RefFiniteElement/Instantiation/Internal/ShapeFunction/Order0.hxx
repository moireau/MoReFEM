///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 4 Jun 2015 16:39:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ShapeFunctionNS
        {



            inline double Order0::ShapeFunction(unsigned int,
                                                const LocalCoords& )
            {
                return 1.;
            }



            inline double Order0::FirstDerivateShapeFunction(unsigned int ,
                                                             unsigned int ,
                                                             const LocalCoords& )
            {
                return 0.;
            }


        } // namespace ShapeFunctionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HXX_
