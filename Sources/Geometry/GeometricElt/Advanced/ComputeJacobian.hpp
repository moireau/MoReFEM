///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_

# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;
    class Coords;
    class LocalCoords;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Advanced
    {


        namespace GeomEltNS
        {


            /*!
             * \brief Helper class to compute Jacobian for a given \a GeometricElt at a given \a LocalCoords (and thus
             * by extension at a given \a QuadraturePoint).
             *
             * The point of this class is to avoid reallocating needlessly memory for the matrix and the list of first
             * derivates values; one such object is intended to compute jacobians for each \a GeometricElt that share
             * the same underlying \a RefGeomElt.
             */
            class ComputeJacobian
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = ComputeJacobian;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] mesh_dimension Dimension in the mesh which \a GeomElt will use current facility. This is
                 * also by construct the number of rows and columns of the jacobian matrix.
                 */
                explicit ComputeJacobian(const unsigned int mesh_dimension);

                //! Destructor.
                ~ComputeJacobian() = default;

                //! Copy constructor.
                ComputeJacobian(const ComputeJacobian&) = delete;

                //! Move constructor.
                ComputeJacobian(ComputeJacobian&&) = delete;

                //! Copy affectation.
                ComputeJacobian& operator=(const ComputeJacobian&) = delete;

                //! Move affectation.
                ComputeJacobian& operator=(ComputeJacobian&&) = delete;

                ///@}


                /*!
                 * \brief Compute the value of the jacobian at \a geometric_elt and \a local_coords.
                 *
                 * \param[in] geom_elt \a GeometricElt considered.
                 * \param[in] local_coords \a LocalCoords for which the computation is performed.
                 *
                 * \return Jacobian matrix.
                 */
                const LocalMatrix& Compute(const GeometricElt& geom_elt,
                                           const LocalCoords& local_coords);

            private:

                /*!
                 * \brief Non constant accessor to the matrix that holds the result of the computation; it should not
                 * be accessible publicly.
                 */
                LocalMatrix& GetNonCstJacobian() noexcept;

                /*!
                 * \brief Non constant accessor to the values of the first derivates of the shape function,
                 * updated along \a jacobian_.
                 *
                 * Likewise, it should not be accessed publicly.
                 *
                 * \return List of first derivate values computed in the last call to Compute().
                 */
                std::vector<double>& GetNonCstFirstDerivateShapeFunction() noexcept;


            private:

                //! Matrix that holds the result of the computation; it should not be accessible publicly.
                LocalMatrix jacobian_;

                /*!
                 * \brief Values of the first derivates of the shape function, updated along \a jacobian_.
                 *
                 * Likewise, it should not be accessed publicly.
                 */
                std::vector<double> first_derivate_shape_function_;


            };


        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/GeometricElt/Advanced/ComputeJacobian.hxx"


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_
