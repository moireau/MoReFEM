///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Mar 2015 17:20:59 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HXX_
# define MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace EnvironmentNS
        {


            template<class T>
            inline std::string GetEnvironmentVariable(T&& variable, const char* invoking_file, int invoking_line)
            {
                static_assert(std::is_same<std::remove_reference_t<T>, std::string>(),
                              "Forwarding reference trick; T is assumed to be a std::string here!");

                return GetEnvironmentVariable(variable.c_str(), invoking_file, invoking_line);
            }


            template<class T>
            inline bool DoExist(T&& variable)
            {
                static_assert(std::is_same<std::remove_reference_t<T>, std::string>(),
                              "Forwarding reference trick; T is assumed to be a std::string here!");

                return DoExist(variable.c_str());
            }


        } // namespace EnvironmentNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HXX_
