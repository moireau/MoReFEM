///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HXX_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            template<class MeshSectionT>
            void GeometricMeshRegionManager::Create(const MeshSectionT& section)
            {
                namespace ipl = Internal::InputParameterListNS;

                decltype(auto) mesh_file = ipl::ExtractPathParameter<typename MeshSectionT::Path>(section);
                decltype(auto) dimension = ipl::ExtractParameter<typename MeshSectionT::Dimension>(section);
                const auto format = FormatNS::GetType(ipl::ExtractParameter<typename MeshSectionT::Format>(section));
                const auto space_unit = ipl::ExtractParameter<typename MeshSectionT::SpaceUnit>(section);

                Create(section.GetUniqueId(),
                       mesh_file,
                       dimension,
                       format,
                       space_unit,
                       GeometricMeshRegion::BuildEdge::yes,
                       GeometricMeshRegion::BuildFace::yes,
                       GeometricMeshRegion::BuildVolume::yes); // \todo #57
            }


            inline GeometricMeshRegion& GeometricMeshRegionManager::GetNonCstMesh(unsigned int unique_id)
            {
                return const_cast<GeometricMeshRegion&>(GetMesh(unique_id));
            }


            template<unsigned int UniqueIdT>
            inline const GeometricMeshRegion& GeometricMeshRegionManager::GetMesh() const
            {
                return GetMesh(UniqueIdT);
            }


            inline const GeometricMeshRegionManager::storage_type& GeometricMeshRegionManager
            ::GetStorage() const noexcept
            {
                return storage_;
            }


            inline GeometricMeshRegionManager::storage_type& GeometricMeshRegionManager
            ::GetNonCstStorage() noexcept
            {
                return const_cast<storage_type&>(GetStorage());
            }


            template<GeometricMeshRegionManager::is_unique_id_known IsUniqueIdKnownT>
            void GeometricMeshRegionManager::InsertMesh(const GeometricMeshRegion* const mesh)
            {
                assert(!(!mesh));
                auto&& ptr = GeometricMeshRegion::const_unique_ptr(mesh);

                const auto unique_id = ptr->GetUniqueId();

                auto&& pair = std::make_pair(unique_id, std::move(ptr));

                auto insert_return_value = storage_.insert(std::move(pair));

                if (!insert_return_value.second)
                    throw Exception("Two geometric mesh region objects can't share the same unique identifier! (namely "
                                    + std::to_string(unique_id) + ").", __FILE__, __LINE__);

                switch(IsUniqueIdKnownT)
                {
                    case is_unique_id_known::yes:
                    {
                        # ifndef NDEBUG
                        {
                            decltype(auto) unique_id_list = GetUniqueIdList();
                            assert(unique_id_list.find(unique_id) != unique_id_list.cend());
                        }
                        # endif // NDEBUG

                        break;
                    }
                    case is_unique_id_known::no:
                    {
                        auto& unique_id_list = GetNonCstUniqueIdList();

                        assert(unique_id_list.find(unique_id) == unique_id_list.cend());
                        unique_id_list.insert(unique_id);
                    }
                }
            }


            inline const std::set<unsigned int>& GeometricMeshRegionManager::GetUniqueIdList() const noexcept
            {
                # ifndef NDEBUG
                {
                    // Check all keys of storage_ are in unique_id_list_ (revert might be false).
                    decltype(auto) storage = GetStorage();

                    for (const auto& pair : storage)
                    {
                        const auto key = pair.first;
                        assert(unique_id_list_.find(key) != unique_id_list_.cend());
                    }
                }
                # endif // NDEBUG


                return unique_id_list_;
            }


            inline std::set<unsigned int>& GeometricMeshRegionManager::GetNonCstUniqueIdList() noexcept
            {
                // Here do not build it on top of GetUniqueIdList(): the assert would not pass during the InsertMesh()
                // call...
                return unique_id_list_;
            }



        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HXX_
