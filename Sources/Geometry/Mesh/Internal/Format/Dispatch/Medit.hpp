///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HPP_

# include <memory>
# include <vector>

# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Medit
                {


                    namespace Dispatch
                    {


                        // ============================
                        //! \cond IGNORE_BLOCK_IN_DOXYGEN
                        // ============================


                        template<class GeoRefEltT>
                        [[noreturn]] GmfKwdCod GetIdentifier(std::false_type);

                        template<class GeoRefEltT>
                        GmfKwdCod GetIdentifier(std::true_type);


                        template<class GeoRefEltT, unsigned int NcoordT>
                        inline void WriteFormat(std::true_type,
                                                         int mesh_index,
                                                         GmfKwdCod geometric_elt_code,
                                                         const std::vector<int>& coords,
                                                         int label_index);

                        template<class GeoRefEltT, unsigned int NcoordT>
                        [[noreturn]] void WriteFormat(std::false_type,
                                                               int mesh_index,
                                                               GmfKwdCod geometric_elt_code,
                                                               const std::vector<int>& coords,
                                                               int label_index);


                        template<class GeoRefEltT, unsigned int NcoordT>
                        inline void ReadFormat(std::true_type,
                                                        int mesh_index,
                                                        GmfKwdCod geometric_elt_code,
                                                        std::vector<unsigned int>& coords,
                                                        int& label_index);

                        template<class GeoRefEltT, unsigned int NcoordT>
                        [[noreturn]] void ReadFormat(std::false_type,
                                                              int mesh_index,
                                                              GmfKwdCod geometric_elt_code,
                                                              std::vector<unsigned int>& coords,
                                                              int& label_index);


                        // ============================
                        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                        // ============================


                    } // namespace Dispatch


                } // namespace Medit


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/Format/Dispatch/Medit.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HPP_
