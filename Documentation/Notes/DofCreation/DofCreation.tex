%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
%
\documentclass[]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
%\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage{latexsym}

% Surround parts of graphics with box
\usepackage{boxedminipage}

% Package for including code in the document
\usepackage{listings}
\usepackage{verbatim}
\usepackage{color}
\usepackage{indentfirst}

% Package for biblio
\usepackage[authoryear,round,comma]{natbib}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                    % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}





% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{Dof creation in HappyHeart}
\author{Sébastien Gilles}

%\date{2013-05-23}

\begin{document}
	
\newcommand{\refchapter}[1]{chapter \ref{#1}}
\newcommand{\refsection}[1]{section \ref{#1}}
\newcommand{\refcode}[1]{code excerpt \ref{#1}}
\newcommand{\path}[1]{\textit{#1}}
\newcommand{\code}[1]{\textit{#1}}
\renewcommand{\lstlistingname}{Code excerpt}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle

\section*{Introduction}

\lstinline{DofManager} is the class in charge of handling the nodes, dofs and unknowns, and to provide quick access to these data (there is for that matter redundancy in its data members to make sure the access is efficient in all the situations).
\medskip

The construction of the content of the class is done in two steps: the call to the constructor, and then the call to the \lstinline{Init()} method. This 2-step structure is required because the latter is in fact a template method, and I didn't want to make \lstinline{DofManager} a template class whereas the template parameters are really used only in the initialization.
\medskip

In the following, I will describe the algorithm involved in the creation of the list of nodes and dofs; I will not consider another responsibility of \lstinline{DofManager} which is the initialization of the boundary conditions.
\medskip

It should be underlined that currently faces and volumes have never been used; the code handles only vertices and edges. However, adding the new interfaces should be quite seamlessly when they will be required.



\section{Creation of the list of nodes (DofManager::CreateNodeList())}

% \subsection{DetermineNdofForNodeNature()}
% 
% First we determine the number of dofs that will be associated with each type of interface. For instance if we consider Stokes problem with quadratic velocity and linear pressure, we will consider in a 3D mesh 4 dofs on vertice and 3 dofs on edges.  

% \subsection{Node creation with Private::CreateNodeListHelper}

\subsection{Prerequisite: GeometricMeshRegion}

The node creation relies on the \lstinline{GeometricMeshRegion}: it assumes that:
\begin{itemize}
    \item The \lstinline{GeometricMeshRegion} is fully built, with the relevant interfaces created if required by the problem.
    \item There is a unique identifier associated to an interface of a given type. So for instance we know there are 0 or 1 Vertex with an index of \lstinline{i}; however an \lstinline{Edge} might also use the same index (there are no united indexes for all interfaces).
\end{itemize}

    
\subsection{Node creation}

For each \lstinline{GeometricElement} in the mesh:

\begin{itemize}
    \item For each vertex in the \lstinline{GeometricElement}:
    \begin{itemize}
        \item Fetch or create the associated \lstinline{Node} (depending on whether it is the first time the vertex is called). If created:
        \begin{itemize}
            \item Specify the nature of the interface.
            \item Determine the number of dofs associated to it.
            \item That's all: the \lstinline{Node} doesn't nothing more at this stage!
            \item Add the new node to \lstinline{DofManager::node_list_}.
        \end{itemize}
        \item Fill \lstinline{DofManager::node_per_geometric_element_}.
    \end{itemize}
    
\end{itemize}

\subsection{First (temporary) program-wise index}

Each \lstinline{Node} is attributed an index which is its position in \lstinline{DofManager::node_list_}. This index is temporary; the definite one is attributed once the partition is done.

    
\section{Partitioning}

\subsection{Crude partitioning}

Parmetis requires in input a dummy partitioning, so the nodes are arbitrarily shared between all processors involved through a simple Euclidian division, with the rest attributed on the last processor.
\medskip

All data continue to be present on all processors; we in fact just assigned a processor to each \lstinline{Node} object.

\subsection{Parmetis call - only in parallel mode}

\subsubsection{Compute connectivity}\label{section_connectivity_matrix}

For each node, compute the list of all other nodes that are on the same processor (according to the current crude partitioning). Self-connexion is excluded here.

\subsubsection{Create a connectivity CSR matrix}

Computes the components of a CSR matrix from the connectivity that has just been computed.

\subsubsection{Call the function to distribute nicely nodes on all processors}

The call is done through a wrapper, which extracts from Parmetis output what we need to define our sparse vectors and matrices. The output is therefore a vector that gives for each node the processor in charge. This vector can be used to call again \lstinline{Node::SetProcessor()} for each node; this time the value won't change further.

\subsection{Assign a program-wise index to each node}

Sort \lstinline{DofManager::node_list_} so that the nodes are sort by increasing processor rank (the internal ordering within a same processor rank is irrelevant).

For each node, assign as its program-wise index the position in \lstinline{DofManager::node_list_}; this index won't change further.


\subsection{Compute dof-related quantities}\label{section_dof_related_quantities}

\lstinline{PartitionHelper::ComputeDofRelatedQuantities()} computes quantities that will be required later in the dof constructor. These quantities are:

\begin{itemize}
    \item \lstinline{DofManager::Nprogram_wise_dof_}
    \item \lstinline{DofManager::Nprocessor_wise_dof_}
    \item For each node, the index of its first program-wise dof index.    
\end{itemize}

\subsection{Prepare the pattern for Petsc matrices} \label{section_matrix_pattern}

\lstinline{PartitionHelper::ComputeMatrixPatternForPetsc()} computes the matrix pattern expected to create a Petsc sparse matrix for the dofs. This format indicates for each row (i.e. for each processor-wise dof) the number of so-called \textit{diagonal} and \textit{off-diagonal} terms.

A diagonal term simply means the column represents a dof that is present on the same processor, whereas an off-diagonal term means the column represents a dof on another processor.

It is important to highlight that there might be more off-diagonal terms than ghosted dofs\footnote{Ghosted terms will be described later in this document.}\dots 

The actual position of the terms is not required by Petsc; it becomes actually locked when the matrix is filled.

This function uses the same function as \refsection{section_connectivity_matrix}, except that this time the self-connexion is kept. This function gives a list of nodes; for each node we know already the number of dofs involved, even if the \lstinline{Dof}
 objects haven't been built yet.
 
\subsection{Reduce the node list to processor-wise data}

Keep only the nodes that are processor-wise in \lstinline{DofManager::node_list}.

\subsection{Attribute GeometricElement to processors}

Each \lstinline{GeometricElement} object is attributed to the processor that manages most of its nodes. If two or more qualifies, the processor that gets the more ghost nodes at the moment of the test is chosen.

\lstinline{DofManager::node_per_geometric_element_} is reduced there to the processor-wise \lstinline{GeometricElement}.

\subsection{Compute ghost nodes}

However, there are nodes that might belong to a \lstinline{GeometricElement} on the current processor that are owned by another processor.

All such nodes are put in \lstinline{DofManager::ghost_node_list_}; the list is filtered at the end of the loop upon \lstinline{DofManager::node_per_geometric_element_} to reduce duplicates.


\subsection{Reduce the GeometricMeshRegion to the processor-wise data}

Finally, the \lstinline{GeometricMeshRegion} object is also reduced to processor-wise data: 
\begin{itemize}
    \item \lstinline{GeometricElement}.
    \item The interfaces.
    \item The labels.
\end{itemize}

At this stage the partition is complete; we can now generate for each processor its dofs (both the processor-wise and the ghosted ones).


\section{Dof creation}

\subsection{Ordering the list of nodes}

Both \lstinline{DofManager::node_list_} and \lstinline{DofManager::ghost_node_list_} are sort by increasing node index.

\subsection{Creation of the dofs}

For each node of \lstinline{DofManager::node_list_}, all the dofs are created. A \lstinline{Dof} is little more than an encapsulation over two indexes:
\lstinline{processor_wise_or_ghost_index_} and \lstinline{program_wise_index_}.

 The rules to build them are the following:

\begin{itemize}
    \item Unknowns are always read in the same order.
    \item Of course, unknowns met won't necessarily be the same depending on the nature of the interface.
    \item Processor-wise index starts at zero for the first dof created and is incremented for each dof.
    \item Program-wise index is built from the informations stored earlier (\refsection{section_dof_related_quantities}).
    
\end{itemize}

Then almost the same procedure is applied to \lstinline{DofManager::ghost_node_list_}, except that the processor-wise index is not reset (its true name is for that matter \lstinline{processor_wise_or_ghost_index_} in \lstinline{Dof} class).

\medskip

The initialization of DofManager is now completely done; however it is interesting to go a bit further and see how Petsc matrix and vectors are built and used:

\section{A step later: Petsc vector and matrices}

Petsc matrices and vectors are currently allocated within \lstinline{LinearProblem::AllocateMatricesAndVectors}.

\subsection{Petsc matrix}

When initializing a Petsc matrix, you need to give to the constructor of the wrapper that calls the original Petsc function:

\begin{itemize}
    \item The number of processor-wise dof.
    \item The number of program-wise dof.
    \item The matrix pattern obtained in \refsection{section_matrix_pattern}.
\end{itemize}

The operations on the matrices (fetching a value inside, zeroing a row when applying boundary conditions, etc\dots) require all the program-wise index.


\subsection{Petsc vector}

To be initialized, a Petsc parallel vector requires:

\begin{itemize}
    \item The number of processor-wise dof.
    \item The number of program-wise dof.
    \item The list of ghosted dofs\footnote{Which can be deduced straightforwardly from the list of \lstinline{DofManager::ghost_node_list_}}.
\end{itemize}

There are two ways to access and modify the internal values, but the most convenient and effective one requires the processor-wise index or the ghost index. What is really convenient is that the same process can be applied to both:

\begin{lstlisting}
    //  vector is a Wrappers::Petsc::Vector defined earlier.

    Wrappers::Petsc::LocalContentGhostVector read_vector_helper(vector, __FILE__, __LINE__);

    for (auto dof_ptr : dof_list)
        // Value is printed whether the dof inhabits the current processor or not.
        std::cout << "The value for the dof is " << read_vector_helper[dof_ptr->GetProcessorWiseOrGhostIndex()] << std::endl;
        
    
\end{lstlisting}



\end{document}