///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 13 Jan 2016 11:18:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace HyperelasticityPolicyNS
            {


                template <class HyperelasticLawT>
                Hyperelasticity<HyperelasticLawT>::Hyperelasticity(const unsigned int mesh_dimension,
                                                                   const HyperelasticLawT* const hyperelastic_law)
                : hyperelastic_law_(hyperelastic_law)
                {
                    switch(mesh_dimension)
                    {
                        case 1u:
                            GetNonCstWorkMatrixOuterProduct().Resize(1, 1);
                            break;
                        case 2u:
                            GetNonCstWorkMatrixOuterProduct().Resize(3, 3);
                            break;
                        case 3u:
                            GetNonCstWorkMatrixOuterProduct().Resize(6, 6);
                            break;
                        default:
                            assert(false);
                            break;
                    }

                    invariant_holder_ = std::make_unique<invariant_holder_type>(mesh_dimension,
                                                                                InvariantHolderNS::Content::invariants_and_first_and_second_deriv);

                    if constexpr (HyperelasticLawT::DoI4Activate())
                    {
                        invariant_holder_->SetFibers(hyperelastic_law_->GetFibers());
                        invariant_holder_->SetI4(true);
                    }
                }


                template <class HyperelasticLawT>
                void Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(const QuadraturePoint& quad_pt,
                                                                          const GeometricElt& geom_elt,
                                                                          const LocalVector& cauchy_green_tensor_value,
                                                                          LocalVector& dW,
                                                                          LocalMatrix& d2W)
                {
                    auto& invariant_holder = this->GetNonCstInvariantHolder();
                    invariant_holder.Update(cauchy_green_tensor_value, quad_pt, geom_elt);

                    const auto& law = GetHyperelasticLaw();

                    const auto& dI1dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI1dC);
                    const auto& dI2dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI2dC);
                    const auto& dI3dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI3dC);

                    const double dWdI1 = law.FirstDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
                    const double dWdI2 = law.FirstDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
                    const double dWdI3 = law.FirstDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);

                    {
                        const auto& d2I2dCdC = invariant_holder.GetSecondDerivativeWrtCauchyGreen(invariant_holder_type::invariants_second_derivative_index::d2I2dCdC);
                        const auto& d2I3dCdC = invariant_holder.GetSecondDerivativeWrtCauchyGreen(invariant_holder_type::invariants_second_derivative_index::d2I3dCdC);

                        {
                            assert(Wrappers::Seldon::IsZeroMatrix(d2W));
                            // d2I1dCdC = d2I4dCdC = 0
                            Seldon::Add(dWdI2, d2I2dCdC, d2W);
                            Seldon::Add(dWdI3, d2I3dCdC, d2W);

                            # ifdef MOREFEM_CHECK_NAN_AND_INF
                            Wrappers::Seldon::ThrowIfNanInside(d2W, __FILE__, __LINE__);
                            # endif // MOREFEM_CHECK_NAN_AND_INF
                        }

                        {
                            const double d2WdI1dI1 =
                                law.SecondDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
                            const double d2WdI2dI2 =
                                law.SecondDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
                            const double d2WdI3dI3 =
                                law.SecondDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);

                            const double d2WdI1dI2 =
                                law.SecondDerivativeWFirstAndSecondInvariant(invariant_holder, quad_pt, geom_elt);
                            const double d2WdI1dI3 =
                                law.SecondDerivativeWFirstAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
                            const double d2WdI2dI3 =
                                law.SecondDerivativeWSecondAndThirdInvariant(invariant_holder, quad_pt, geom_elt);

                            using namespace Wrappers::Seldon;

                            auto& outer_prod = GetNonCstWorkMatrixOuterProduct();

                            OuterProd(dI1dC, dI1dC, outer_prod);
                            Seldon::Add(d2WdI1dI1, outer_prod, d2W);

                            OuterProd(dI1dC, dI2dC, outer_prod);
                            Seldon::Add(d2WdI1dI2, outer_prod, d2W);

                            OuterProd(dI1dC, dI3dC, outer_prod);
                            Seldon::Add(d2WdI1dI3, outer_prod, d2W);

                            OuterProd(dI2dC, dI1dC, outer_prod);
                            Seldon::Add(d2WdI1dI2, outer_prod, d2W);

                            OuterProd(dI2dC, dI2dC, outer_prod);
                            Seldon::Add(d2WdI2dI2, outer_prod, d2W);

                            OuterProd(dI2dC, dI3dC, outer_prod);
                            Seldon::Add(d2WdI2dI3, outer_prod, d2W);

                            OuterProd(dI3dC, dI1dC, outer_prod);
                            Seldon::Add(d2WdI1dI3, outer_prod, d2W);

                            OuterProd(dI3dC, dI2dC, outer_prod);
                            Seldon::Add(d2WdI2dI3, outer_prod, d2W);

                            OuterProd(dI3dC, dI3dC, outer_prod);
                            Seldon::Add(d2WdI3dI3, outer_prod, d2W);

                            if constexpr (HyperelasticLawT::DoI4Activate())
                            {
                                const auto& dI4dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI4dC);

                                const double d2WdI1dI4 =
                                law.SecondDerivativeWFirstAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
                                const double d2WdI2dI4 =
                                law.SecondDerivativeWSecondAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
                                const double d2WdI3dI4 =
                                law.SecondDerivativeWThirdAndFourthInvariant(invariant_holder, quad_pt, geom_elt);

                                const double d2WdI4dI4 =
                                law.SecondDerivativeWFourthInvariant(invariant_holder, quad_pt, geom_elt);

                                OuterProd(dI4dC, dI1dC, outer_prod);
                                Seldon::Add(d2WdI1dI4, outer_prod, d2W);

                                OuterProd(dI4dC, dI2dC, outer_prod);
                                Seldon::Add(d2WdI2dI4, outer_prod, d2W);

                                OuterProd(dI4dC, dI3dC, outer_prod);
                                Seldon::Add(d2WdI3dI4, outer_prod, d2W);

                                OuterProd(dI1dC, dI4dC, outer_prod);
                                Seldon::Add(d2WdI1dI4, outer_prod, d2W);

                                OuterProd(dI2dC, dI4dC, outer_prod);
                                Seldon::Add(d2WdI2dI4, outer_prod, d2W);

                                OuterProd(dI3dC, dI4dC, outer_prod);
                                Seldon::Add(d2WdI3dI4, outer_prod, d2W);

                                OuterProd(dI4dC, dI4dC, outer_prod);
                                Seldon::Add(d2WdI4dI4, outer_prod, d2W);
                            }
                        }

                        Seldon::Mlt(4., d2W);
                    }


                    {
                        assert(Wrappers::Seldon::IsZeroVector(dW));
                        Seldon::Add(dWdI1, dI1dC, dW);
                        Seldon::Add(dWdI2, dI2dC, dW);
                        Seldon::Add(dWdI3, dI3dC, dW);

                        if constexpr (HyperelasticLawT::DoI4Activate())
                        {
                            const double dWdI4 = law.FirstDerivativeWFourthInvariant(invariant_holder, quad_pt, geom_elt);
                            const auto& dI4dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI4dC);

                            Seldon::Add(dWdI4, dI4dC, dW);
                        }

                        Seldon::Mlt(2., dW);
                    }

                    #ifndef NDEBUG
                    invariant_holder.Reset();
                    #endif // NDEBUG
                }


                template <class HyperelasticLawT>
                inline const typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&
                Hyperelasticity<HyperelasticLawT>
                ::GetInvariantHolder() const noexcept
                {
                    assert(!(!invariant_holder_));
                    return *invariant_holder_;
                }



                template <class HyperelasticLawT>
                inline typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&
                Hyperelasticity<HyperelasticLawT>
                ::GetNonCstInvariantHolder() noexcept
                {
                    return const_cast<typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&>(GetInvariantHolder());
                }


                template <class HyperelasticLawT>
                inline LocalMatrix& Hyperelasticity<HyperelasticLawT>::GetNonCstWorkMatrixOuterProduct() noexcept
                {
                    return work_matrix_outer_product_;
                }


                template <class HyperelasticLawT>
                inline const HyperelasticLawT& Hyperelasticity<HyperelasticLawT>::GetHyperelasticLaw() const noexcept
                {
                    assert(!(!hyperelastic_law_));
                    return *hyperelastic_law_;
                }


            } // namespace HyperelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_
