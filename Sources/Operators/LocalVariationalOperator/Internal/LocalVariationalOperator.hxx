///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:15:35 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>
            ::LocalVariationalOperator(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                       const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                       elementary_data_type&& elementary_data)
            : extended_unknown_and_test_unknown_list_parent(unknown_list, test_unknown_list),
            elementary_data_(std::move(elementary_data))
            { }


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>::~LocalVariationalOperator() = default;


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            constexpr Advanced::OperatorNS::Nature LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>
            ::GetOperatorNature()
            {
                return OperatorNatureT;
            }


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            inline const typename LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>::elementary_data_type&
            LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>
            ::GetElementaryData() const
            {
                return elementary_data_;
            }


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            inline typename LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>::elementary_data_type&
            LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>
            ::GetNonCstElementaryData()
            {
                return elementary_data_;
            }


            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            void LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>
            ::SetLocalFEltSpace(const LocalFEltSpace& local_felt_space)
            {
                auto& elementary_data = GetNonCstElementaryData();
                elementary_data.ComputeLocalFEltSpaceData(local_felt_space);
                elementary_data.Zero();
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HXX_
