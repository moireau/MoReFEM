///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 18:23:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include "Utilities/InputParameterList/ExtendedOps.hpp"



namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            ExtendedOps::ExtendedOps(const std::string& file_path)
            : ::Ops::Ops(file_path)
            { }


            void ExtendedOps::ClearExceptState()
            {
                ClearPrefix();
                read_bool.clear();
                read_int.clear();
                read_float.clear();
                read_double.clear();
                read_string.clear();
                read_vect_bool.clear();
                read_vect_int.clear();
                read_vect_float.clear();
                read_vect_double.clear();
                read_vect_string.clear();
            }


        } //  namespace InputParameterListNS
        
        
    } // namespace Utilities
    
    
} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
