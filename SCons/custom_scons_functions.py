import os
import sys
import subprocess
import SCons
import imp


def PrepareTargetName(source, suffix_to_remove = ""):
    """
    \param[in] source A File object, such as returned by scons Library() or Program().
    \param[in] suffix_to_remove For executable, we can build in Intermediate build two different executables:
    hyperelasticity-static and hyperelasticity-shared. However in the Build there are two different folders
    for static and shared, and the name can simply be hyperelasticity. suffix_to_remove is therefore "-static"
    or "-shared"; correct one is yielded by "-{0}".format(env["LIBRARY_TYPE"]).
    
    This function returns the name of the program or the library, without the path.    
    """
    
    assert(len(source) == 1)
    source_file = str(source[0])
 
    ret = os.path.split(source_file)[1]
    
    if suffix_to_remove:
        assert ret.endswith(suffix_to_remove)
        pos = len(ret) - len(suffix_to_remove)        
        ret = ret[:pos]
        
    return ret
    


def InstallStaticLibrary(target, source, env):
    """
    This function is expected to be used through Scons 'Command'; hence its prototype.

    \param[in] target List of Scons object targets; for this specific function the number of targets should be one.
    \param[in] source List of Scons object sources; for this specific function the number of sources should be one.
    \param[in] env Scons object environment.

    The static library is copied into a target folder.
    """
    # Copy the library file.
    assert(len(source) == 1)
    assert(len(target) == 1)

    src_name = str(source[0])
    target_name = str(target[0])

    env.Execute(SCons.Defaults.Copy(target_name, src_name))


def InstallDynamicLibrary(target, source, env):
    """
    This function is expected to be used through Scons 'Command'; hence its prototype.
    
    \param[in] target List of Scons object targets; for this specific function the number of targets should be one.
    \param[in] source List of Scons object sources; for this specific function the number of sources should be one.
    \param[in] env Scons object environment.    
    
    The dynamic library is copied into a target folder and its content is edited so that the libraries can be used from anywhere (absolute path are put instead of previous relative ones).
    """
    InstallStaticLibrary(target, source, env)
    
    target_name = str(target[0])
    
    # Then run install_name_tool if you're on Mac OS X.
    if env['PLATFORM'] == 'darwin':
        cmd = ("install_name_tool", "-id", os.path.abspath(target_name), target_name)
        subprocess.Popen(" ".join(cmd), shell = True).communicate()
            
    
    
def InstallExecutable(target, source, env):
    """
    Copy the executable into the install folder.
    
    See InstallDynamicLibrary() for a more thorough explanation of each term.    
    """  
    
    # Copy the executable file.
    assert(len(source) == 1)
    assert(len(target) == 1)

    src_name = str(source[0])
    target_name = str(target[0])
    
    env.Execute(SCons.Defaults.Copy(target_name, src_name))
    
    


def CreateObjectList(env, source_list):
    """Go through the list of source files and ask for each of them for the creation of an object.
    
    \param[in] source_list List of source files to consider.
    \return List of objects (as SCons objects; names can be derived with str()).
    """
    ret = []
    
    for source_file in source_list:
        try:
            obj_file = env.Object(source_file)
        except:
            # ThirdParty add some flags to disable warnings; to objects are given instead of sources.
            # In this case the call above raise an exception; I assume here it is because \a source_file
            # is already an object.
            obj_file = source_file
            
        ret.append(obj_file)
    
    return ret
    
    
def Prelink(target, source, env):
    """Function that defines the namesake builder.
    
    \param[in] target Single object file that agglomerate all those entailed for the library. The reason of
    that step is that in MoReFEM some symbols must be resolved before linking (the ones related to the
    factories).
    \param[in] source List of objects as created by CreateObjectList() function.
    \param[in] env Environment. Not used but signature of this function is constrained by SCons builders.
    
    \return None SCons requested return value when the target could be properly created.
    """
    assert len(target) == 1
    
    object_name_list = []
    
    for my_object in source:
        name = str(my_object)
        object_name_list.append(name)

    cmd = "ld -r {0} -o {1}".format(" ".join(object_name_list), str(target[0]))

    subprocess.Popen(cmd, shell = True).communicate()

    return None
    
    
def CustomStaticLibrary(env, name, source_file_list):
    """Create a static library where all the objects have been prelinked in a single one.
    
    The reason for this is that MoReFEM use a FactoryPattern which loads data in the factory before the 
    program even starts. With a standard StaticLibrary, these informations aren't kept and the program fails in runtime.
    
    The library does the same operation as 'Perform Single-Object Prelink = yes' option in XCode settings.
    """
    
    # First create the list of objects.
    object_list = CreateObjectList(env, source_file_list)

    # Call the Prelink builder to create a single object file that covers the whole library.
    single_obj_file = "prelink_{0}.o".format(name)
    env.Prelink(single_obj_file, object_list)

    # Create the static library from the result of the Prelink builder.
    return env.StaticLibrary(name, single_obj_file,
                             LIBS=env["LINK_LIBRARIES_LIST"],
                             LIBPATH=env['LIBPATH'])
                             


def MoReFEMLibrary(env, name, source_file_list, suppl_link_libraries = None):
    """Build a library from a list of sources; its nature depends on env["LIBRARY_TYPE"].

    \param[in] env Environment.
    \param[in] name Name of the library to build. Don't bother with extensions: SCons handles them itself.
    \param[in] source_file_list List of source (or object in fact) files to include in the library.
    \param[in] suppl_link_libraries List of additional libraries required by the library (env["LINK_LIBRARIES_LIST"] already covers the libraries shared by all MoReFEM libraries. Only relevant for shared libraries.
    
    \return The Library as a SCons node object.
    """

    if env["LIBRARY_TYPE"] == "static":
        return CustomStaticLibrary(env, name, source_file_list)
    else:
        libs = []
    
        if suppl_link_libraries:
            libs = suppl_link_libraries
        
        libs.extend(env["LINK_LIBRARIES_LIST"])
        
        return env.SharedLibrary(name, source_file_list, LIBS=libs, LIBPATH=env['LIBPATH'])
        
        
        
def MoReFEMProgram(env, name, source_file, suppl_link_libraries = None):
    """Build a program.

    \param[in] env Environment.
    \param[in] name Name of the program to build. Don't bother with extensions: SCons handles them itself. 
    A -static or -shared is added to this name in IntermediateBuild, but does not appear in final build.
    \param[in] source_file The source file that include the main (and if you wish other cpp files as well in fact).
    \param[in] suppl_link_libraries List of additional libraries required by the program (env["LINK_LIBRARIES_LIST"] already covers the libraries shared by any MoReFEM program).
    
    \return The Program as a SCons node object.
    """
    libs = []
    
    if suppl_link_libraries:
        libs = suppl_link_libraries
        
    libs.extend(env["LINK_LIBRARIES_LIST"])
    
    program = env.Program('{0}-{1}'.format(name, env["LIBRARY_TYPE"]), 
                          source_file,
                          LIBS=libs,
                          LIBPATH=env['LIBPATH'])
                           
    env.Alias(name, os.path.join(env["TARGET_VARIANT_DIR"], name))
    
    return program
    
    
def DefineEnvironment(vars, morefem_root = None):
    """Define the SCons environment object.
    
    \param[in] morefem_root Path to the MoReFEM root folder (named 'MoReFEM'). If empty, a default value 
    defined from current location is attempted.
    
    It is there that compilation flags are set; informations are taken from dedicated folders sit in current
    MoReFEM/SCons directory.
    """
    env = SCons.Environment.Environment(variables = vars,
                                        CFLAGS = ['-std=c99'],
                                        CXXFLAGS = ['-std=c++1z'])
                  
                  
    #env['ENV']['TERM'] = os.environ['TERM']
    
    if not morefem_root:
        env['MOREFEM_ROOT'] = os.path.join(os.getcwd(), '..')
    else:
        env['MOREFEM_ROOT'] = morefem_root
    
    
    env['MOREFEM_SCONS'] = os.path.join(env['MOREFEM_ROOT'], 'SCons')

    macro_flags = [
        'OPS_WITH_EXCEPTION', 
        'SELDON_WITH_LAPACK', 
        'SELDON_WITH_BLAS',
        'SELDON_WITH_COMPILED_LIBRARY'    
    ]

    # I assume here the compiler displays options close to gcc ones... If not some rewrite should be involved here.
    if env['MODE'] == 'debug':
        macro_flags.extend(['DEBUG=1', 'SELDON_CHECK_BOUNDS', 'SELDON_CHECK_DIMENSIONS'])
        optimization_flags = ['-g', ]
        env['PETSC_MODE_INCL_DIR'] = env['PETSC_DEBUG_INCL_DIR']
        env['PETSC_MODE_LIB_DIR'] = env['PETSC_DEBUG_LIB_DIR']
        
        if env['MODE'] == 'full_debug':
            macro_flags.append('MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE')        
    else:
        macro_flags.append('NDEBUG')
        optimization_flags=['-O3', ]
        env['PETSC_MODE_INCL_DIR'] = env['PETSC_RELEASE_INCL_DIR']
        env['PETSC_MODE_LIB_DIR'] = env['PETSC_RELEASE_LIB_DIR']
        
    # Add specific macros if asked in configuration file.
    specific_macro_list = \
    ( \
        "MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE", \
        "MOREFEM_EXTENDED_TIME_KEEP", \
        "MOREFEM_CHECK_NAN_AND_INF"
    )
    
    for entry in specific_macro_list:
        if env[entry]:
            macro_flags.append(entry)
    
    if env['MODE'] == 'callgrind':
        optimization_flags.append('-g')
    
    #---------------------------------------------------------------------------------------
    # Read and load informations relevant for the chosen compiler.
    #---------------------------------------------------------------------------------------
    scons_compiler_folder = os.path.realpath(os.path.join(env['MOREFEM_SCONS'], 'Compilers'))

    assert(os.path.exists(scons_compiler_folder)) # If not, SConstruct does not reflect MoReFEM architecture.    

    try:
        compiler_module = imp.load_source(env['COMPILER'], '{0}/{1}.py'.format(scons_compiler_folder, \
                                                                               env['COMPILER']))
    
        warning_flags = compiler_module.WarningFlags()
        miscellaneous_flags = compiler_module.MiscellaneousFlags(env)
    
        if env['MODE'] == 'debug':
            macro_flags.extend(compiler_module.DebugMacroFlags())
        
    except Exception as e:
    
        print("Exception found: {0}".format(e))
    
        compiler_list = [file for file in os.listdir(scons_compiler_folder) if os.path.splitext(file)[1] == ".py"]
        compiler_list = [compiler[:-3] for compiler in compiler_list]
    
        print("[ERROR] Compiler suite ('{0}') is not yet foreseen in current Scons build; only available compilers are those detailed in {1}: "\
              "{2}.".format(env['COMPILER'],
                            scons_compiler_folder, \
                            compiler_list))
    
        env.Exit(1)
          

    #---------------------------------------------------------------------------------------
    # Set the compiler flags from what has been gathered previously.
    #---------------------------------------------------------------------------------------

    env.Append(CFLAGS=optimization_flags)                      

    env.Append(CXXFLAGS=optimization_flags)
    env.Append(CXXFLAGS=warning_flags)
    env.Append(CXXFLAGS=miscellaneous_flags)

    env.Append(CPPDEFINES=macro_flags)


    # Add to the environment the Prelink builder.
    builder = SCons.Builder.Builder(action=Prelink)
    env.Append(BUILDERS = {'Prelink': builder})

    # List of include directories.
    include_dirs = [
        os.path.join(env['MOREFEM_ROOT'], 'Sources'),
        os.path.join(env['MOREFEM_ROOT'], 'Sources', 'ThirdParty', 'Source'),
        os.path.join(env['MOREFEM_ROOT'], 'Sources', 'ThirdParty', 'Source', 'Tclap', 'include'),
        env['OPEN_MPI_INCL_DIR'],
        env['PETSC_GENERAL_INCL_DIR'],
        env['PETSC_MODE_INCL_DIR'],
        env['PARMETIS_INCL_DIR'],
        env['LUA_INCL_DIR'],
        env['BOOST_INCL_DIR'],
        os.path.join(env['MOREFEM_ROOT'], 'Sources', 'ThirdParty', 'Source', 'Ops'),
        os.path.join(env['MOREFEM_ROOT'], 'Sources', 'ThirdParty', 'Source', 'Seldon')
    ]    
    
    if env['PHILLIPS_DIR']:
        include_dirs.append(env['PHILLIPS_DIR'])

    for folder in include_dirs:
        if not os.path.isdir(folder):
            print("[ERROR] Include path {0} doesn't exist or is not a folder.".format(folder))
            env.Exit(1)

    # List of library paths.
    lib_dirs = [
        env['OPEN_MPI_LIB_DIR'],
        env['PETSC_MODE_LIB_DIR'],  
        env['PARMETIS_LIB_DIR'],
        env['LUA_LIB_DIR'],
        env['BOOST_LIB_DIR']
    ]
    
    if env['PHILLIPS_LINKER']:
        lib_dirs.append(env['PHILLIPS_DIR'])

    for folder in lib_dirs:
        if not os.path.isdir(folder):
            print("[ERROR] Library path {0} doesn't exist or is not a folder.".format(folder))
            env.Exit(1)
    
    # Blas directory is handled separately, as it might not be specified (in OS X where framework Accelerate is used for Blas).
    if env['BLAS_LIB_DIR']:
        lib_dirs.append(env['BLAS_LIB_DIR'])

    env.Append(CPPPATH = include_dirs)
    env.Append(LIBPATH = lib_dirs)


    # List of the third-party libraries used in the code.
    env['THIRD_PARTY_LIBRARIES'] = SCons.Util.Split('''
        petsc
        boost_filesystem
        boost_system
        lua
        parmetis
        metis
        mpi
        ''')

    if not env['BLAS_CUSTOM_LINKER']:
        env['THIRD_PARTY_LIBRARIES'].append(env['BLAS_LIB'])
    else:
        env.Append(LINKFLAGS=env['BLAS_LIB'])
        
    if env['PHILLIPS_LINKER']:
        env['THIRD_PARTY_LIBRARIES'].append('pfa_border_distance_interface')

    env.Append(LIBS=env['THIRD_PARTY_LIBRARIES'])


    #---------------------------------------------------------------------------------------
    # Create the intermediate and final build directories.
    #---------------------------------------------------------------------------------------
    env['CURRENT_BUILD_INTERMEDIATE_DIR'] = os.path.join(env['INTERMEDIATE_BUILD_DIR'], env['COMPILER_DIRECTORY'], env['MODE'])
    env.Execute(SCons.Defaults.Mkdir(env['CURRENT_BUILD_INTERMEDIATE_DIR']))

    # Directory a user should use to fetch the program. The issues about relative paths for dynamic libraries are for instance solved in this installation folder.
    env["TARGET_VARIANT_DIR"] = os.path.realpath(os.path.join(env['BUILD_DIR'], env['COMPILER_DIRECTORY'], env['MODE'], env["LIBRARY_TYPE"]))
    env.Execute(SCons.Defaults.Mkdir(env["TARGET_VARIANT_DIR"]))

    env.Append(LIBPATH = env["TARGET_VARIANT_DIR"])


    #----------------------------------------------------------------------
    # Add link flag so that the dynamic libraries may be found at runtime.
    #----------------------------------------------------------------------
    if env['LIBRARY_TYPE'] == "shared":
        for path in env['LIBPATH']:
            env.Append(LINKFLAGS=['-Wl,-rpath,{0}'.format(path)])

    #-----------------------------------------------------------------------------------------------------
    # Create an alias: type 'scons all' in command line to populate fully the env["TARGET_VARIANT_DIR"].
    # 'scons' alone is not enough, because I choose an installation directory outside of source folder.
    #-----------------------------------------------------------------------------------------------------
    env.Alias('all', env["TARGET_VARIANT_DIR"])
    
    return env
    
    
    
def BuildModel(env, model_name, all_instances_executable_list, all_instances_library_list, suppl_link_libraries = None):
    """Call the SConscript of a given Model.
    
     \param[in] suppl_link_libraries List of additional libraries required by the program (env["LINK_LIBRARIES_LIST"] already covers the libraries shared by any MoReFEM program).
    """
    if not suppl_link_libraries:
        (executable_list, library_list) = env.SConscript(os.path.join(model_name, 'SConscript'), ['env'], duplicate=0)
    else:
        (executable_list, library_list) = env.SConscript(os.path.join(model_name, 'SConscript'), ['env', 'suppl_link_libraries'], duplicate=0)
    
    for executable in executable_list:
        all_instances_executable_list.append(executable)        

    for library in library_list:
        all_instances_library_list.append(library)
    
    
def AggregateLibraries(target, source, env):
    """
    This function is expected to be used through Scons 'Command'; hence its prototype.
    
    \param[in] target List of Scons object targets; for this specific function the number of targets should be one.
    \param[in] source List of Scons object sources; for this specific function the number of sources should be one.
    \param[in] env Scons object environment.    
    
    The point here is to aggregate several libraries into one, using libtool for that.
    """
    assert len(target) == 1
    
    if env["LIBRARY_TYPE"] == "static":
        type_mode = "-static"
    else:
        type_mode = "-dynamic"
        
    source_list = [str(item) for item in source]
        
    cmd = ("libtool", type_mode, "-o", os.path.abspath(str(target[0])), " ".join(source_list))
    
    # Unfortunately the libtool called with shell = False returns an error, hence the shell = True.
    #subprocess.Popen(cmd, shell = False).communicate()
    subprocess.Popen(" ".join(cmd), shell = True).communicate()
    
