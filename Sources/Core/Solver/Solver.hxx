///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HXX_
# define MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HXX_


namespace MoReFEM
{


    template<unsigned int SolverIndexT, class InputParameterDataT>
    Wrappers::Petsc::Snes::unique_ptr InitSolver(const Wrappers::Mpi& mpi,
                                                 const InputParameterDataT& input_parameter_data,
                                                 Wrappers::Petsc::Snes::SNESFunction snes_function,
                                                 Wrappers::Petsc::Snes::SNESJacobian snes_jacobian,
                                                 Wrappers::Petsc::Snes::SNESViewer snes_viewer,
                                                 Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function)
    {
        namespace ipl = Utilities::InputParameterListNS;
        using ip_petsc = InputParameter::Petsc<SolverIndexT>;

        decltype(auto) solver = ipl::Extract<typename ip_petsc::Solver>::Value(input_parameter_data);
        const std::string& preconditioner = ipl::Extract<typename ip_petsc::Preconditioner>::Value(input_parameter_data);

        const double absolute_tolerance = ipl::Extract<typename ip_petsc::AbsoluteTolerance>::Value(input_parameter_data);
        const double relative_tolerance = ipl::Extract<typename ip_petsc::RelativeTolerance>::Value(input_parameter_data);
        const double step_size_tolerance = ipl::Extract<typename ip_petsc::StepSizeTolerance>::Value(input_parameter_data);
        const unsigned int gmres_restart = ipl::Extract<typename ip_petsc::GmresRestart>::Value(input_parameter_data);
        const unsigned int max_iteration = ipl::Extract<typename ip_petsc::MaxIteration>::Value(input_parameter_data);

        return std::make_unique<Wrappers::Petsc::Snes>(mpi,
                                                       solver,
                                                       preconditioner,
                                                       gmres_restart,
                                                       absolute_tolerance,
                                                       relative_tolerance,
                                                       step_size_tolerance,
                                                       max_iteration,
                                                       snes_function,
                                                       snes_jacobian,
                                                       snes_viewer,
                                                       snes_convergence_test_function,
                                                       __FILE__, __LINE__);
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HXX_
