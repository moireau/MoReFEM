///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HXX_


namespace MoReFEM
{


    namespace NonConformInterpolatorNS
    {


        template<class InputParameterDataT>
        FromVertexMatching::FromVertexMatching(const InputParameterDataT& input_parameter_data,
                                               const unsigned int source_index,
                                               const unsigned int target_index,
                                               store_matrix_pattern do_store_matrix_pattern)
        # ifndef NDEBUG
        : do_store_matrix_pattern_(do_store_matrix_pattern)
        # endif // NDEBUG
        {
            MeshNS::InterpolationNS::VertexMatching vertex_matching( input_parameter_data);

            Construct(vertex_matching,
                      source_index,
                      target_index,
                      do_store_matrix_pattern);
        }


        inline const GlobalMatrix& FromVertexMatching::GetInterpolationMatrix() const noexcept
        {
            assert(!(!interpolation_matrix_));
            return *interpolation_matrix_;
        }


        inline const Wrappers::Petsc::MatrixPattern& FromVertexMatching::GetMatrixPattern() const noexcept
        {
            assert(do_store_matrix_pattern_ == store_matrix_pattern::yes && "If 'no', present method shouldn't be called.");
            assert(!(!matrix_pattern_));
            return *matrix_pattern_;
        }


    } // namespace NonConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HXX_
