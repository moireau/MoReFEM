///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Apr 2015 12:13:39 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElementSpace/Internal/MatrixPattern.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            MatrixPattern::MatrixPattern(const NumberingSubset::const_shared_ptr& row_numbering_subset_ptr,
                                         const NumberingSubset::const_shared_ptr& column_numbering_subset_ptr,
                                         const connectivity_type& connectivity,
                                         const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                         const NdofHolder& Ndof_holder)
            : row_numbering_subset_(row_numbering_subset_ptr),
            column_numbering_subset_(column_numbering_subset_ptr)
            {
                // Now fill the number of diagonal and non-diagonal terms per row (consider only processor-wise dofs).
                
                assert(!(!row_numbering_subset_ptr));
                assert(!(!column_numbering_subset_ptr));
                const auto& row_numbering_subset = *row_numbering_subset_ptr;
                const auto& column_numbering_subset = *column_numbering_subset_ptr;
                
                std::vector<std::vector<PetscInt>> content_for_each_local_row;
                content_for_each_local_row.resize(Ndof_holder.NprocessorWiseDof(row_numbering_subset));
                
                std::size_t buf_dof_index = 0;
                
                assert(processor_wise_node_bearer_list.size() == connectivity.size());
                assert(std::is_sorted(processor_wise_node_bearer_list.cbegin(),
                                      processor_wise_node_bearer_list.cend(),
                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));
                
                #ifndef NDEBUG
                {
                    NodeBearer::vector_shared_ptr keys;
                    keys.reserve(connectivity.size());
                    
                    for (const auto& item : connectivity)
                        keys.push_back(item.first);
                    
                    std::sort(keys.begin(), keys.end(), Utilities::PointerComparison::Less<NodeBearer::shared_ptr>());
                    
                    assert(keys == processor_wise_node_bearer_list);
                }
                #endif // NDEBUG
                
                for (const auto& node_bearer_ptr : processor_wise_node_bearer_list)
                {
                    assert(!(!node_bearer_ptr));
                    
                    auto it = connectivity.find(node_bearer_ptr);
                    
                    assert(it != connectivity.cend() && "This method is called after reduction to processor-wise of the "
                           "node bearer list.");
                    
                    // Compute the list of dofs that are connected to the current dof. As node_bearer is itself in
                    // connected list, dofs on the same node_bearer are counted as well.
                    std::vector<PetscInt> program_wise_dof_indexes_in_local_row;
                    {
                        for (const auto& connected_node_bearer_ptr : it->second)
                        {
                            assert(!(!connected_node_bearer_ptr));
                            const auto& connected_node_bearer = *connected_node_bearer_ptr;
                            
                            const auto& node_storage = connected_node_bearer.GetNodeList();
                            
                            for (const auto& node_ptr : node_storage)
                            {
                                assert(!(!node_ptr));
                                const auto& node = *node_ptr;
                                
                                if (!node.DoBelongToNumberingSubset(column_numbering_subset))
                                    continue;
                                
                                const auto& dof_list = node.GetDofList();
                                
                                for (const auto& dof_ptr : dof_list)
                                {
                                    assert(!(!dof_ptr));
                                    program_wise_dof_indexes_in_local_row.push_back(static_cast<PetscInt>(dof_ptr->GetProgramWiseIndex(column_numbering_subset)));
                                }
                            }
                        }
                    }
                    
                    
                    // Sort in increasing order, to comply with CSR format.
                    std::sort(program_wise_dof_indexes_in_local_row.begin(), program_wise_dof_indexes_in_local_row.end());
                    
                    
                    // Now copy the content in the rows.
                    {
                        const auto& node_list = node_bearer_ptr->GetNodeList();
                        
                        for (const auto& node_ptr : node_list)
                        {
                            assert(!(!node_ptr));
                            
                            const auto& node = *node_ptr;
                            
                            if (!node.DoBelongToNumberingSubset(row_numbering_subset))
                                continue;
                            
                            const unsigned int Ndof = node_ptr->Ndof();
                            
                            for (unsigned int i = 0; i < Ndof; ++i)
                            {
                                auto row_index = buf_dof_index++;
                                assert(row_index < content_for_each_local_row.size());
                                assert(content_for_each_local_row[row_index].empty() && "A local row should be filled only once!");
                                
                                content_for_each_local_row[row_index] = program_wise_dof_indexes_in_local_row;
                            }
                        }
                    }
                }
        
                matrix_pattern_ =
                    std::make_unique<::MoReFEM::Wrappers::Petsc::MatrixPattern>(content_for_each_local_row);
            }
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
