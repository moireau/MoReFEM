///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ModelGroup
/// \addtogroup ModelGroup
/// \{

#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
# define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_

# include <memory>
# include <vector>
# include <map>

# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
# include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"


namespace MoReFEM
{



    namespace Internal
    {


        namespace ModelNS
        {


            /*!
             * \brief Create the finite element spaces and init the god of dofs with them.
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
             * \param[in] output_directory_per_mesh_index Key is the unique id of the \a GeometricMeshRegion (same
             * as the one for the \a GodOfDof), value the path to the associated output directory (which should have
             * already been created by \a Model class).
             */
            template<class InputParameterDataT>
            void InitGodOfDof(const InputParameterDataT& input_parameter_data,
                              DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                              std::map<unsigned int, std::string>&& output_directory_per_mesh_index);



            /*!
             * \brief Create all the finite element spaces.
             *
             * \internal <b><tt>[internal]</tt></b> Should not be called outside of InitGodOfDof().
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             * \param[out] felt_space_list_per_god_of_dof_index Instantiate all \a FEltSpace detailed in the input
             * parameter file and store them in this container which key is the index of the \a GodOfDof (which are
             * not yet created when this function is called).
             */
            template<class InputParameterDataT>
            void CreateFEltSpaceList(const InputParameterDataT& input_parameter_data,
                                     std::map<unsigned int, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index);


            /*!
             * \brief Init each god of dof with its finite element spaces.
             *
             * \internal <b><tt>[internal]</tt></b> Should not be called outside of InitGodOfDof().
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
             * \copydoc doxygen_hide_do_create_output_dir_arg
             *
             * \param[in] felt_space_list_per_god_of_dof_index List of all \a FEltSpace sort \a GodOfDof index. FEltSpace
             * have already been instantiated during this call, and are given to their proper \a GodOfDof.
             * The reference is not constant due to move semantics inside, but the output value shouldn't be considered.
             */
            template<class InputParameterDataT>
            void InitEachGodOfDof(const InputParameterDataT& input_parameter_data,
                                  std::map<unsigned int, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                                  DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                                  create_output_dir do_create_output_dir);


        } // namespace ModelNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


# include "Model/Internal/InitializeHelper.hxx"


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
