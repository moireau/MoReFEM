///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template<unsigned int IndexT>
        const std::string& Unknown<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("Unknown", IndexT);
            return ret;
        };


        template<unsigned int IndexT>
        constexpr unsigned int Unknown<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HXX_
