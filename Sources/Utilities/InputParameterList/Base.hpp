///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HPP_


# include <vector>
# include <string>
# include <cassert>
# include <tuple>
# include <type_traits>

# include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "Utilities/Containers/Tuple.hpp"
# include "Utilities/Filesystem/File.hpp"
# include "Utilities/Filesystem/Folder.hpp"
# include "Utilities/String/String.hpp"
# include "Utilities/Environment/EnvironmentVariable.hpp"
# include "Utilities/InputParameterList/ExtendedOps.hpp"
# include "Utilities/InputParameterList/Exceptions/InputParameterList.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/TupleIteration.hpp"
# include "Utilities/InputParameterList/Internal/ManualParsing/ManualParsing.hpp"
# include "Utilities/InputParameterList/Internal/Subtuple/Subtuple.hpp"
# include "Utilities/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /// \namespace MoReFEM::Utilities::InputParameterListNS
        /// \brief Namespace dedicated to InputParameterList.
        namespace InputParameterListNS
        {


            /*!
             * \brief An enum class that will basically act as a boolean.
             *
             * InputParameterList::Base class takes into account which input parameters are actually used; the
             * way to do so is to set a bit in a bitset when some methods are called. However, sometimes these
             * methods might be called to some purposes that do not truly mean the input parameter is used.
             *
             * For instance, if we check at the beginning of the program that several input vectors are the same
             * length it doesn't mean all of them are actually used, hence the following enum to be able to
             * specify the method not to set the bit for this specific use.
             *
             */
            enum class CountAsUsed
            {
                no,
                yes
            };



            //! Behaviour when the folder read in the input parameter file doesn't exist.
            enum class UnexistentFolderPolicy
            {
                create,
                throw_exception
            };


            /*!
             * \brief Whether a field found in the file but not referenced in the tuple yields an exception or not.
             *
             * Should be 'yes' most of the time; I have introduced the 'no' option for cases in which we need only
             * a handful of parameters shared by all models for post-processing purposes (namely mesh-related ones).
             */
            enum class DoTrackUnusedFields { yes, no };


            // ============================
            // Forward declarations.
            // ============================

            template<class ObjectT>
            struct Extract;


            // ============================
            // End of forward declarations.
            // ============================



            /*!
             * \brief Provides all the mechanisms to read the input parameters and hold their values.
             *
             * As it names hints, it is intended to be a base class for a user-defined class which will deals
             * with the input parameters specific to the user problem.
             *
             * \tparam DerivedT Derived CRTP class.
             * \tparam TupleT A tuple which should include a class for each input parameter to consider in the file.
             * The class for each input parameter should derive from InputParameter::Crtp::InputParameter and defines several
             * static methods such as Description(), NameInFile() or Section(). Compiler won't let you not define one
             * of those anyway; to see an example of such a class see in Core/InputParameter or in MoReFEM documentation.
             *
             * \internal <b><tt>[internal]</tt></b> It should be a specialization of InputParameter; however I didn't manage to make it
             * a template template parameter that works both on clang and gcc.
             */
            template<class DerivedT, class TupleT>
            class Base : public ::MoReFEM::Crtp::CrtpMpi<Base<DerivedT, TupleT>>
            {

            public:


                //! Friendship.
                template<class ObjectT>
                friend struct Extract;


            public:

                //! Mpi parent.
                using mpi_parent = ::MoReFEM::Crtp::CrtpMpi<Base<DerivedT, TupleT>>;

                //! The underlying tuple type.
                using Tuple = TupleT;

                //! Size of the tuple.
                static constexpr std::size_t Size();

                //! Special members.
                //@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] filename Name of the input parameter list to be read. This file is expected to be formatted
                 * in Ops format.
                 * \param[in] do_track_unused_fields Whether a field found in the file but not referenced in the tuple
                 * yields an exception or not. Should be 'yes' most of the time; I have introduced the 'no' option for
                 * cases in which we need only a handful of parameters shared by all models for post-processing
                 * purposes (namely mesh-related ones).
                 */
                explicit Base(const std::string& filename,
                              const Wrappers::Mpi& mpi,
                              DoTrackUnusedFields do_track_unused_fields = DoTrackUnusedFields::yes);

                //! Disable copy constructor.
                Base(const Base&) = delete;

                //! Disable move constructor.
                Base(Base&&) = delete;

                //! Disable copy assignation.
                Base& operator=(const Base&) = delete;

                //! Disable move assignation.
                Base& operator=(Base&&) = delete;


                //! Destructor.
                ~Base();
                //@}


                /*!
                 * \brief Print the list of input parameter that weren't used in the program.
                 *
                 * This method is dedicated to be called at destruction, but when using OpenMPI it is rather uneasy
                 * to make it called in the destructor due to racing conditions (error if MPI_Finalize() has already
                 * been called.
                 *
                 * So this method should be called at the very end of your program if you want the information
                 * it gives.
                 *
                 * \param[in,out] out Stream to which the output will be printed. If nothing added then all input
                 * parameters were used.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method collects the data from all mpi processors; the list is therefore ensured to
                 * be exhaustive.
                 *
                 */
                void PrintUnused(std::ostream& out) const;

                //! Get the path to the input parameter file.
                const std::string& GetInputFile() const;


            private:


                /*!
                 * \brief Helper function used to extract quantity from the tuple.
                 *
                 * This should not be used directly by a user; ExtractValue class deals with it with a more
                 * user-friendly interface (no pesky template keyword to add!).
                 *
                 * \tparam InputParameterT InputParameter class used to store the wanted input parameter.
                 * \tparam CountAsUsedT Whether the call to the methods counts as an effective use of the input
                 * parameter in the program. See CountAsUsed for more details; default value is fine
                 * in almost all cases.
                 *
                 * \return The requested element in the tuple. It is either copied by value or a const reference.
                 */
                template<class InputParameterT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                typename Utilities::ConstRefOrValue<typename InputParameterT::return_type>::type
                ReadHelper() const;


                /*!
                 * \brief Helper function used to extract a value from a vector element from the tuple.
                 *
                 * This should not be used directly by a user; Extract class deals with it with a more
                 * user-friendly interface (no pesky template keyword to add!).
                 *
                 * When an input parameter is a vector, this method may be used to fetch an element of this vector.
                 *
                 * \param[in] index Index in the vector of the quantity fetched.
                 * \tparam CountAsUsedT See ReadHelper().
                 *
                 * \return The requested element in the tuple. It is either copied by value or a const reference.
                 */
                template<class InputParameterT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                typename Utilities::ConstRefOrValue<typename InputParameterT::return_type::value_type>::type
                ReadHelper(unsigned int index) const;


                /*!
                 * \brief Helper function used to extract the size of a vector input parameter.
                 *
                 * \return Size of the vector read as input parameter.
                 *
                 */
                template<class InputParameterT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                unsigned int ReadHelperNumber() const;



                /*!
                 * \brief Helper function used to handle a folder path: in case it doesn't exist, it can be
                 * created or an exception might be thrown (depending on the policy chosen).
                 *
                 * \return The requested element in the tuple. This string is afterwards interpreted as a folder;
                 * \a UnexistentFolderPolicyT policy tells what to do when the folder doesn't exist.
                 *
                 */
                template<class InputParameterT, UnexistentFolderPolicy UnexistentFolderPolicyT = UnexistentFolderPolicy::create>
                std::string ReadHelperFolder() const;


                /*!
                 * \brief Helper function used to handle a path: it is basically handled as a mere string, except
                 * that environment variables are replaced on the fly if they respect the format ${...}.
                 *
                 * \warning Contrary to what the const subject (there to give the same interface whatever the string input
                 * parameter is), this method might modify the value stored in the tuple. On first call all environment
                 * variables are replaced, and what is stored afterwards is the result after the substitution.
                 *
                 * A specialized version for folder also exist (ReadHelperFolder()).
                 *
                 * For instance "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
                 *
                 * \return The requested element in the tuple.
                 */
                template<class InputParameterT>
                std::string ReadHelperPath() const;



            private:

                /*!
                 * \brief Ensure all input parameters in \a SubTupleT address vectors with the same size.
                 */
                template<class SubTupleT>
                void EnsureSameLength() const;



                /*!
                 * \brief Throw an exception if there is a duplicate in the keys.
                 *
                 * The key for an InputParameter A class is given by free function InputParameterKey<A>().
                 *
                 * As these keys are what is used by Ops to read the value, it is important not to use the same for two
                 * parameters.
                 *
                 * An additional bonus is that a same InputParameter class put twice in the tuple will also trigger this
                 * exception.
                 */
                void CheckNoDuplicateKeysInTuple() const;


                /*!
                 * \brief Parse the input file and check it seems sound.
                 *
                 * \param[in] filename Path to the input file.
                 * \param[in] do_track_unused_fields See constructor.
                 *
                 * This method is called prior to any call to Ops (and therefore to Lua as well).
                 *
                 * \internal <b><tt>[internal]</tt></b> This code is temporary and should be replaced in due time by proper Lua code, but as I am
                 * not fluent in Lua I am not able to fulfill two requirements for InputParameterList:
                 * - Forbid duplicates in the  keys of the input parameter file. If left to Lua, when one variable
                 * is assigned twice second value is considered.
                 * - Indicate the keys that are useless.
                 *
                 * If one of these requirements is not fulfilled an exception is thrown.
                 *
                 */
                void CheckInputFile(const std::string& filename, DoTrackUnusedFields do_track_unused_fields) const;

            public:

                //! Accessor to the underlying tuple.
                const auto& GetTuple() const;

            private:

                //! Number of elements in the tuple.
                enum { tuple_size_ = std::tuple_size<Tuple>::value };


                //! The tuple that actually stores all the relevant quantities for the problem.
                Tuple tuple_;

                /*!
                 * \brief Keep track of which input parameter was actually used in the program.
                 *
                 * The bitset ordering is exactly the same as the tuple, so if bitset[10] is set, it simply means
                 * the 11th element of the Tuple was actually used.
                 */
               // mutable std::bitset<tuple_size_> used_;

                /*!
                 * \brief Store the underlying Ops object.
                 *
                 * \internal <b><tt>[internal]</tt></b> It is still required past construction to deal with functions defined in the input parameter file:
                 * it is Ops which handle the calls to Lua to provide the function outputs.
                 */
                ExtendedOps::unique_ptr ops_ = nullptr;

                //! Path of the input parameter file.
                std::string input_parameter_file_;

                //! Helper object to iterate upon the tuple (including the in-depth sections).
                using tuple_iteration = Internal::InputParameterListNS::TupleIteration<TupleT, 0, tuple_size_>;

            };


            /*!
             * \brief Create a default input file, with all relevant input parameters provided in \a TupleT.
             *
             * If a default parameter is provided in InputParameter class, it will be written in the file, otherwise
             * a filler text will be written to remind it should be filled.
             *
             * \tparam TupleT including all the input parameters we want to consider.
             *
             * \param[in] path Path to the file that will contain the result. An exception is thrown if the path is invalid.
             */
            template<class TupleT>
            void CreateDefaultInputFile(const std::string& path);


            /*!
             * \brief A convenient helper to extract information from Base class.
             *
             * The problem with Base class is that the syntax is really hideous; for instance a derived class
             * from base (named IPL for the example below) must use the following syntax to extract a value
             * named Component:
             *
             * \code
             * using Type = InputParameter::BoundaryCondition::Component;
             * auto component_value = this->template ReadHelper<Type>();
             * \endcode
             *
             * This template syntax is quite unusual and many C++ developers don't know it; so the helper class
             * hids that and the same quantity may be extracted with:
             * \code
             * using Type = InputParameter::BoundaryCondition::Component;
             * auto component_value = Extract<Type>::Value();
             * \endcode
             *
             * This class can also be used to read Lua functions:
             * \code
             * decltype(auto) fct = Extract<InputParameter::Force::Surfacic>::Value(input_parameter_data);
             * std::cout << "Value = " << fct(0, 1, 2, 3) << std::endl;
             * \endcode
             */
            template<class ObjectT>
            struct Extract
            {

                //! Convenient alias over the return type.
                using return_type = typename ObjectT::return_type;

                //! Return the value of the input parameter.
                template<class InputParameterDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                static typename ConstRefOrValue<return_type>::type Value(const InputParameterDataT& input_parameter_list);


                /*!
                 * \brief Return a string that stands for a path; only additional operation is to replace environment
                 * variables by their value.
                 *
                 * For instance "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
                 *
                 * \param[in] input_parameter_list Object which holds all relevant input data for the model considered.
                 *
                 * \return Path related to \a ObjectT.
                 */
                template<class InputParameterDataT>
                static std::string Path(const InputParameterDataT& input_parameter_list);


                /*!
                 * \brief Return a string that stands for a folder; several operations might be attempted if said folder
                 * doesn't exist.
                 *
                 * \param[in] input_parameter_list Object which holds all relevant input data for the model considered.
                 *
                 * \return Path to the folder hold by \a ObjectT.
                 */
                template<class InputParameterDataT, UnexistentFolderPolicy UnexistentFolderPolicyT = UnexistentFolderPolicy::create>
                static std::string Folder(const InputParameterDataT& input_parameter_list);


                /*!
                 * \brief Read the number of elements when the object is a vector.
                 *
                 * If not a vector, a compilation error will occur as size() method won't be defined.
                 * \param[in] input_parameter_list Object which hols all relevant input data for the model considered.
                 *
                 *
                 * \return Number of elements hold by \a ObjectT.

                 */
                template<class InputParameterDataT>
                static unsigned int Number(const InputParameterDataT& input_parameter_list);

                /*!
                 * \brief Return the \a index -th value of a vector.
                 *
                 * If the input parameter is not a vector, subscript operator will yield an error at compile time.
                 *
                 * \param[in] input_parameter_list Object which holds all relevant input data for the model considered.
                 * \param[in] index Index of the element sought in the vector read in the \a input_parameter_list.
                 *
                 * \return Value of the \a index -th element of \a ObjectT.
                 */
                template<class InputParameterDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                static decltype(auto) Subscript(const InputParameterDataT& input_parameter_list,
                                                unsigned int index);

            };


        } //  namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Internal/ExtractParameter/ExtractParameter.hpp"
# include "Utilities/InputParameterList/Base.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HPP_
