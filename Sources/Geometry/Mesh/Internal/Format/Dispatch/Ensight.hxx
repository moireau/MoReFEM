///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Ensight
                {


                    namespace Dispatch
                    {


                        template<class GeoRefElementT>
                        [[noreturn]] const std::string& GetName(std::false_type)
                        {
                            throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(GeoRefElementT::ClassName(),
                                                                                           "Ensight",
                                                                                           __FILE__, __LINE__);
                        }


                        template<class GeoRefElementT>
                        const std::string& GetName(std::true_type)
                        {
                            static auto ret =
                                Internal::MeshNS::FormatNS::Support
                                <
                                    ::MoReFEM::MeshNS::Format::Ensight,
                                    GeoRefElementT::Identifier()
                                >::EnsightName();

                            return ret;
                        }



                        template<class GeoRefElementT, unsigned int NcoordT>
                        void WriteFormat(std::true_type,
                                         std::ostream& stream,
                                         bool do_print_index, unsigned int index,
                                         const Coords::vector_raw_ptr& coords_list)
                        {
                            if (do_print_index)
                                stream << std::setw(8) << index;

                            for (const auto& coord_ptr : coords_list)
                            {
                                assert(!(!coord_ptr));
                                stream << std::setw(8) << coord_ptr->GetIndex();
                            }

                            stream << '\n';
                        }


                        template<class GeoRefElementT, unsigned int NcoordT>
                        [[noreturn]] void WriteFormat(std::false_type,
                                                      std::ostream& /*stream*/,
                                                      bool /*do_print_index*/, unsigned int /*index*/,
                                                      const Coords::vector_raw_ptr& /*coords_list*/)
                        {
                            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(GeoRefElementT::ClassName(),
                                                                                            "Ensight",
                                                                                            __FILE__, __LINE__);
                        }


                    } // namespace Dispatch


                } // namespace Ensight


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_
