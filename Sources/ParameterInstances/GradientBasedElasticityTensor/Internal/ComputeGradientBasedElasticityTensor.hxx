///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 16:10:44 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            ComputeGradientBasedElasticityTensor<ConfigurationT>::ComputeGradientBasedElasticityTensor()
            {
                result_.Resize(traits::result_size, traits::result_size);
                engineering_elasticity_tensor_.Resize(traits::engineering_size, traits::engineering_size);
                engineering_elasticity_tensor_.Zero();
                intermediate_product_.Resize(traits::engineering_size, traits::result_size);
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>
            ::GetNonCstResult()
            {
                return const_cast<LocalMatrix&>(GetResult());
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>
            ::GetResult() const noexcept
            {
                assert(result_.GetM() == traits::result_size);
                assert(result_.GetN() == traits::result_size);

                return result_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>
            ::GetNonCstEngineeringElasticityTensor()
            {
                assert(engineering_elasticity_tensor_.GetM() == traits::engineering_size);
                assert(engineering_elasticity_tensor_.GetN() == traits::engineering_size);

                return engineering_elasticity_tensor_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>
            ::GetNonCstIntermediateProduct()
            {
                assert(intermediate_product_.GetM() == traits::engineering_size);
                assert(intermediate_product_.GetN() == traits::result_size);

                return intermediate_product_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>
            ::Compute(const double young_modulus, const double poisson_ratio)
            {
                const auto& engineering_elasticity_tensor =
                ComputeEngineeringElasticityTensor(young_modulus, poisson_ratio);

                const auto& gradient_2_strain = Gradient2Strain<traits::dimension>();
                const auto& transpose_gradient_2_strain = TransposeGradient2Strain<traits::dimension>();

                auto& intermediate_product = GetNonCstIntermediateProduct();
                Seldon::Mlt(engineering_elasticity_tensor, gradient_2_strain, intermediate_product);

                auto& result = GetNonCstResult();
                Seldon::Mlt(transpose_gradient_2_strain, intermediate_product, result);

                return result_;
            }



        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
