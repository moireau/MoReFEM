///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include <string>

#include "Core/InputParameter/Geometry/Impl/LightweightDomainList.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        namespace Impl
        {
            
            
            namespace LightweightDomainListNS
            {
                
                
                const std::string& MeshIndex::NameInFile()
                {
                    static std::string ret("mesh_index");
                    return ret;
                }
                
                
                const std::string& MeshIndex::Description()
                {
                    static std::string ret("Index of the mesh ontpo which current domains are defined.");
                    return ret;
                }
                
                
                const std::string& MeshIndex::Constraint()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& MeshIndex::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& NumberInDomainList::NameInFile()
                {
                    static std::string ret("number_in_domain_list");
                    return ret;
                }
                
                
                const std::string& NumberInDomainList::Description()
                {
                    static std::string ret("Number of mesh labels to consider in each domain. Sum of these numbers "
                                           "must be equal to the number of entries in mesh label list.");
                    
                    return ret;
                }
                
                
                const std::string& NumberInDomainList::Constraint()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                
                const std::string& NumberInDomainList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& DomainIndexList::NameInFile()
                {
                    static std::string ret("domain_index_list");
                    return ret;
                }
                
                
                const std::string& DomainIndexList::Description()
                {
                    static std::string ret("Give an unique id to each of the shorthand domains defined. These must not "
                                           "clas with each other or with domains defined by a more conventional way.");
                    return ret;
                }
                
                
                const std::string& DomainIndexList::Constraint()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& DomainIndexList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
            } // namespace LightweightDomainListNS
            
            
        } // namespace Impl
        
        
    } // namespace InputParameter
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
