///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 11 Oct 2016 14:00:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HXX_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {

            template
            <
                ::MoReFEM::ParameterNS::Type TypeT,
                template<::MoReFEM::ParameterNS::Type> class TimeDependencyT
            >
            void StaticIf<TypeT, TimeDependencyT>
            ::InitNoTimeDependencyHelper(typename TimeDependencyT<TypeT>::unique_ptr& time_dependency)
            {
                // Do nothing!
                static_cast<void>(time_dependency);
            }



            template
            <
                ::MoReFEM::ParameterNS::Type TypeT
            >
            void StaticIf<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>
            ::InitNoTimeDependencyHelper(typename none_type::unique_ptr& time_dependency)
            {
                time_dependency = std::make_unique<none_type>();
            }


            template
            <
                ::MoReFEM::ParameterNS::Type TypeT,
                template<::MoReFEM::ParameterNS::Type> class TimeDependencyT
            >
            typename StaticIf<TypeT, TimeDependencyT>::return_type
            StaticIf<TypeT, TimeDependencyT>
            ::ApplyTimeFactor(return_type value_without_time_factor,
                              const TimeDependencyT<TypeT>& time_dependency_object)
            {
                return time_dependency_object.ApplyTimeFactor(value_without_time_factor);
            }


            template<::MoReFEM::ParameterNS::Type TypeT>
            typename StaticIf<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>::return_type
            StaticIf<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>
            ::ApplyTimeFactor(return_type value_without_time_factor,
                              const none_type& unused)
            {
                static_cast<void>(unused);
                return value_without_time_factor;
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HXX_
