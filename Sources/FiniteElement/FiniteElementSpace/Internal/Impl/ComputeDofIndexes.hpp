///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Oct 2014 10:36:28 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
# include "FiniteElement/Unknown/UnknownManager.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            //! The three kinds of indexes that may be associated to a Dof.
            enum class TypeDofIndex
            {
                program_wise_per_numbering_subset,
                processor_wise_per_numbering_subset,
                processor_wise
            };


            namespace Impl
            {


                //! Helper enum for ComputeDofIndexesHelper (see below).
                enum class DoProduceDofList { yes, no };



                /*!
                 * \brief The class holding the method that will actually compute the dof indexes for all the nodes of a list.
                 *
                 * There is a struct to ease friendship declaration.
                 *
                 */
                struct ComputeDofIndexesHelper final
                {


                    /*!
                     * \brief Static method that does the actual work.
                     *
                     * \tparam TypeDofIndexT Whether we are computing program_wise or processor (or ghost)-wise indexes.
                     * \tparam DoProduceDofListT Whether \a complete_dof_list is filled or not.
                     *
                     * \param[in] node_bearer_list List of node bearers for which dof indexes are computed. It is expected the structure
                     * of the node bearers (its nodes, the exact number of dofs) is already initialized.
                     * \param[in] dof_numbering_scheme When considering a vectorial unknown, this argument reflects the choice
                     * of numbering. Two possibilities at the moment: components of a same Node are given contiguous indexes
                     * or on the contrary all dofs related to a same component (for instance displacement/x) are contiguous,
                     * then all of the following component.
                     * \param[in,out] current_dof_index In input, the starting point of the numbering. In output, the highest
                     * dof index attributed. The reason for this is simply for processor-wise and ghosts: there are two
                     * consecutive calls, the first on node on processor, the second on ghosted nodes, and the numbering
                     * of ghosts is expected to begin where the one of node ended.
                     * \param[in] numbering_subset_ptr Pointer to a numbering subset if computation is performed for
                     * a numbering subset. If not, put nullptr and no numbering subset filtering will be applied.
                     * \param[out] complete_dof_list List of all dofs created, in the order of their creation. Only if
                     * DoProduceDofListT is yes.
                     */
                    template<TypeDofIndex TypeDofIndexT, DoProduceDofList DoProduceDofListT>
                    static void Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                        DofNumberingScheme dof_numbering_scheme,
                                        const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                                        unsigned int& current_dof_index,
                                        Dof::vector_shared_ptr& complete_dof_list);


                };


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/Impl/ComputeDofIndexes.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HPP_
