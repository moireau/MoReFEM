//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "Operators/LocalVariationalOperatorInstances/___VARIABLE_operatorNature___Form/___FILEBASENAME___.hpp"


namespace MoReFEM
{
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        ___FILEBASENAMEASIDENTIFIER___::___FILEBASENAMEASIDENTIFIER___(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                                                       elementary_data_type&& a_elementary_data,
                                                                       // \TODO Add if any same supplementary arguments as those given to GlobalVariationalOperator counterpart.
        : ___VARIABLE_operatorNature___LocalVariationalOperator(a_unknown_storage, std::move(a_elementary_data)),
        Crtp::LocalMatrixStorage<___FILEBASENAMEASIDENTIFIER___, // Use same parent as in jhpp if relevant>()
        Crtp::LocalVectorStorage<___FILEBASENAMEASIDENTIFIER___, // Use same parent as in jhpp if relevant>()
                                                               // \TODO If any supplementary argument, they should land here.)
        {
            // \TODO allocate here all the local matrices defined in Crtp::LocalMatrixStorage (if relevant).
            InitLocalMatrixStorage({
                {
                    { M1, N1 },
                    { M2, N2 }, ...
                }
            });

            // \TODO allocate here all the local vectors defined in Crtp::LocalVectorStorage (if relevant).
            InitLocalVectorStorage({
                {
                    size1,
                    size2 ...
                    
                }
            });
              
        
        }
                                                                       
                                                                       
        ___FILEBASENAMEASIDENTIFIER___::~___FILEBASENAMEASIDENTIFIER___() = default;
        
                                                                       
        const std::string& ___FILEBASENAMEASIDENTIFIER___::ClassName()
        {
            static std::string name("___FILEBASENAMEASIDENTIFIER___");
            return name;
        }
        
        
        void ___FILEBASENAMEASIDENTIFIER___::ComputeEltArray(
                                                                                                 // \TODO Add any LOCAL supplementary argument here.)
        {
            // \TODO The core of the definition of the operator. Have a look to an existing one to get the gist of it.
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM
