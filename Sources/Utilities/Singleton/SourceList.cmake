target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Singleton.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Singleton.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
