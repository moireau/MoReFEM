///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 18:23:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXTENDED_OPS_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXTENDED_OPS_HPP_


# include <memory>

# include "ThirdParty/IncludeWithoutWarning/Ops/Ops.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            /*!
             * \brief A limited extension to Ops.
             *
             * It just adds a method to clear everything except Lua state: past the built of InputParameterList::Base,
             * Ops is required only to calculate functions given as input parameters. Other quantities are stored
             * elsewhere, in the tuple of InputParameterList::Base.
             */
            class ExtendedOps final : public ::Ops::Ops
            {

            public:

                //! Smart pointer.
                using unique_ptr = std::unique_ptr<ExtendedOps>;

            public:

                /// \name Special members.
                ///@{

                //! Constructor from a file.
                explicit ExtendedOps(const std::string& file_path);

                //! Disable copy constructor.
                ExtendedOps(const ExtendedOps&) = delete;

                //! Disable move constructor.
                ExtendedOps(ExtendedOps&&) = delete;

                //! Disable copy assignation.
                ExtendedOps& operator=(const ExtendedOps&) = delete;

                //! Disable move assignation.
                ExtendedOps& operator=(ExtendedOps&&) = delete;

                //! Destructor.
                ~ExtendedOps() = default;

                ///@}

                /*!
                 * \brief Clear everything except the state.
                 */
                void ClearExceptState();
            };


        } //  namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXTENDED_OPS_HPP_
