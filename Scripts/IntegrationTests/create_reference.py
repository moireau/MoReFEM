import os

import manual_test
import targets



if __name__ == "__main__":
    
    target_list = targets.TestAndModelTargetList()
    
    morefem_path = os.path.join("/Users", os.environ['USER'], "Codes", "MoReFEM")   
    
    cases = ('clang_debug', 'clang_release', 'gcc_debug', 'gcc_release', 'clang_debug_macro', 'llvm_clang_debug')
    
    for case in cases:
        
        stdout = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "test_stdout_{case}.txt".format(case = case))
        stderr = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "test_stderr_{case}.txt".format(case = case))
    
        if os.path.exists(stdout):
            os.remove(stdout)
        
        if os.path.exists(stderr):
            os.remove(stderr)
            
        log_file = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "log_file_{case}.txt".format(case = case))    
    
        manual_test.CheckMoReFEM(morefem_path,
                                    os.path.join(morefem_path, "Sources", "build_configuration_{case}.py".format(case = case)),
                                    target_list,
                                    log_file,
                                    stdout,
                                    stderr,
                                    do_create_reference = True)
