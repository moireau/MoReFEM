function indices_active_dofs = RemoveInactiveDofs(all_indices, indices_inactive_dofs)

nDofs = length(all_indices);
indices_dofs = 1:nDofs;
nDofs_BC = length(indices_inactive_dofs);
indices_active_dofs = indices_dofs;
% Reduction of the actives dofs by deleting those from Dirichlet BC.
for i=1:nDofs_BC
    indices_active_dofs = indices_active_dofs(indices_active_dofs~=indices_inactive_dofs(i));
end

end