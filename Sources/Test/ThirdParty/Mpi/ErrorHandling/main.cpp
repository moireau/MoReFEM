/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/Mpi/ErrorHandling/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = TestNS::InputParameterListNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        //const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        const auto rank = mpi.GetRank<int>();
        assert(mpi.Nprocessor<int>() == 3 && "This test has been written for three processors specifically!");

        try
        {
            namespace ipl = Utilities::InputParameterListNS;
            
            std::vector<int> sent_data;
            
            if (rank == 0)
                sent_data = { 1, 2, 3 };
            if (rank == 1)
                sent_data = { 11, 12, 13 };
            if (rank == 2)
                sent_data = { 21, 22, 23, 24 };
            
            
            std::vector<int> gathered_data;

            // This should fail: processor 2 should abort the program.
            // Unfortunately, this is not what we got with the error handling provided by
            // MPI_Comm_set_errhandler(communicator, MPI_ERRORS_ARE_FATAL);
            mpi.Gather(sent_data, gathered_data);
            
            if (mpi.IsRootProcessor())
                Utilities::PrintContainer(gathered_data);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());            
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }        
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

