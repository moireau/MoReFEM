///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Mar 2015 14:57:33 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT, std::size_t NlocalVectorT>
        constexpr std::size_t LocalVectorStorage<DerivedT, NlocalVectorT>::N()
        {
            return NlocalVectorT;
        }


        template<class DerivedT, std::size_t NlocalVectorT>
        void LocalVectorStorage<DerivedT, NlocalVectorT>
        ::InitLocalVectorStorage(const std::array<unsigned int, NlocalVectorT>& vectors_dimension)
        {
            for (std::size_t i = 0ul; i < NlocalVectorT; ++i)
            {
                auto& vector = vector_list_[i];
                vector.Resize(static_cast<int>(vectors_dimension[i]));
                vector.Zero();
            }
        }


        template<class DerivedT, std::size_t NlocalVectorT>
        template<std::size_t IndexT>
        inline LocalVector& LocalVectorStorage<DerivedT, NlocalVectorT>::GetLocalVector() const
        {
            static_assert(IndexT < NlocalVectorT, "Check index is within bounds!");
            return vector_list_[IndexT];
        }



    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_
