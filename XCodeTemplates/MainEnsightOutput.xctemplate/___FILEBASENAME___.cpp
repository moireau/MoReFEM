//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"
#include "PostProcessing/PostProcessing.hpp"

#include "ModelInstances/___VARIABLE_groupName:identifier___/Model.hpp"
#include "ModelInstances/___VARIABLE_groupName:identifier___/InputParameterList.hpp"


using namespace MoReFEM;
using namespace MoReFEM::___VARIABLE_problemName:identifier___NS;


int main(int argc, char** argv)
{
    
    try
    {
        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = ___VARIABLE_problemName:identifier___NS::InputParameterList;
        
        MoReFEMData<InputParameterList, Utilities::InputParameterListNS::DoTrackUnusedFields::no> morefem_data(argc, argv);
        
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            namespace ipl = Utilities::InputParameterListNS;
            
            using Result = InputParameter::Result;
            decltype(auto) result_directory = ipl::Extract<Result::OutputDirectory>::Folder(input_parameter_data);
            
            if (!FilesystemNS::Folder::DoExist(result_directory))
                throw Exception("The specified directory doesn't exist!", __FILE__, __LINE__);
            
            decltype(auto) mesh_manager = Internal::MeshNS::GeometricMeshRegionManager::CreateOrGetInstance();
            MoReFEM::Advanced::SetFromInputParameterData<>(input_parameter_data, mesh_manager);
            
            const GeometricMeshRegion& mesh = mesh_manager.GetMesh(EnumUnderlyingType(MeshIndex::/* Specify here mesh unique id; you may duplicate this line if several meshes involved. */));
            
            {
                decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
                MoReFEM::Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
            }
            
            {
                std::vector<unsigned int> numbering_subset_id_list
                {
                    EnumUnderlyingType(NumberingSubsetIndex::/* Specify here numbering subset id for which you want Ensight output */)
                };
                
                std::vector<std::string> unknown_list
                {
                    /* Specify here the name of the unknowns for which you want Ensight output. They should be the
                     same number as in numbering_subset_id_list; if you need two unknowns extracted from the same
                     numbering subset, specify it twice in numbering_subset_id_list. */
                };
                
                PostProcessingNS::OutputFormat::Ensight6 ensight_output(result_directory,
                                                                        unknown_list,
                                                                        numbering_subset_id_list,
                                                                        mesh);
            }
            
            std::cout << "End of Post-Processing." << std::endl;
            std::cout << TimeKeep::GetInstance().TimeElapsedSinceBeginning() << std::endl;
            
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

