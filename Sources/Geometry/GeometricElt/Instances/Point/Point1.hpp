///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Point/Point1.hpp"
//# include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Point1 geometric element read in a mesh.
     *
     * We are not considering here a generic Point1 object (that's the role of MoReFEM::RefGeomEltNS::Point1), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Point1 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Point1>
    {
    public:

        //! Minimal constructor: only the GeometricMeshRegion is provided.
        explicit Point1(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Point1(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::istream& stream);

        //! Constructor from vector of coords.
        explicit Point1(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Point1() override;

        //! Copy constructor.
        Point1(const Point1&) = default;

        //! Move constructor.
        Point1(Point1&&) = default;

        //! Copy affectation.
        Point1& operator=(const Point1&) = default;

        //! Move affectation.
        Point1& operator=(Point1&&) = default;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Point1& StaticRefGeomElt();



    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
