///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Dec 2016 15:47:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            template<class ParameterT, class DerivedT, class EnclosingSectionT, CountAsUsed CountAsUsedT>
            decltype(auto) ExtractParameter(const Utilities::InputParameterListNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
            {
                const auto& section_content_tuple = section.GetSectionContent();

                using tuple_type = std::decay_t<decltype(section_content_tuple)>;

                const auto& parameter = std::get<Utilities::Tuple::IndexOf<ParameterT, tuple_type>::value>(section_content_tuple);

                if (CountAsUsedT == CountAsUsed::yes)
                    parameter.SetAsUsed();

                return parameter.GetTheValue();
            }


            template<class ParameterT, class DerivedT, class EnclosingSectionT, CountAsUsed CountAsUsedT>
            std::string ExtractPathParameter(const Utilities::InputParameterListNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
            {
                auto path_read = ExtractParameter<ParameterT>(section);
                return Utilities::EnvironmentNS::SubstituteValues(path_read);
            }


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HXX_
