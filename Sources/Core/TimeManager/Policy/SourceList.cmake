target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hxx"
)

