//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/OpsFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"

# include "ModelInstances/___VARIABLE_relativePath___/InputParameterList.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation
        : public MoReFEM::VariationalFormulation
        <
            VariationalFormulation,
            EnumUnderlyingType(/* put the enum class value that indicates which solver is used */)
        >
        // TODO If there are sources involved, add the following line:
        // ,public Crtp::VolumicAndSurfacicSource<VariationalFormulation>
        {
        private:
            
            //! \copydoc doxygen_hide_alias_self
            // \TODO This might seem a bit dumb but is actually very convenient for template classes.
            using self = VariationalFormulation;
            
            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation
            <
                self,
                EnumUnderlyingType(/* put the enum class value that indicates which solver is used */)
            >;
            
            
            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;
            
        public:
            
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;
            
        public:
    
            /// \name Special members.
            ///@{
    
            //! copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& numbering_subset1,
                                            const NumberingSubset& numbering_subset2, // TODO and more if needed...
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);
    
            //! Destructor.
            ~VariationalFormulation() = default;
    
            //! Copy constructor.
            VariationalFormulation(const VariationalFormulation&) = delete;
    
            //! Move constructor.
            VariationalFormulation(VariationalFormulation&&) = delete;
    
            //! Copy affectation.
            VariationalFormulation& operator=(const VariationalFormulation&) = delete;
            
            //! Move affectation.
            VariationalFormulation& operator=(VariationalFormulation&&) = delete;
    
            ///@}
            
           
        private:
    
            /// \name CRTP-required methods.
            ///@{
    
            //! \copydoc doxygen_hide_varf_suppl_init
            // TODO: InputParameterDataT might probably be replaced by the actual InputParameterList used in the Model.
            template<class InputParameterDataT>
            void SupplInit(const InputParameterDataT& input_parameter_data);
    
            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();
    
            //! Define the pointer function required to calculate the function required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;
    
            //! Define the pointer function required to calculate the jacobian required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;
    
            //! Define the pointer function required to view the results required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;
            
            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

    
    
            ///@}
            
        private:
            
            
            /// \name Global variational operators.
            ///@{
            
            // \TODO Define there as data attributes the global operators involved in the formulation.
            // They should be defined as std::unique_ptr; accessors that returns a reference to them should also be
            // foreseen.
        
            ///@}
            
        private:
            
            /// \name Global vectors and matrices specific to the problem.
            ///@{
            
            // \TODO Define here the GlobalMatrix and GlobalVector objects into which the global operators are 
            // assembled.
            
            ///@}
            
        private:
            
            
            /// \name Numbering subsets used in the formulation.
            ///@{
            
            // \TODO Define here one or several attributes of the type const NumberingSubset&.
            const NumberingSubset& numbering_subset1_;
            
            const NumberingSubset& numbering_subset2_;
            
            // \TODO ...

            ///@}

            
    
        };
    
    
    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM


# include "ModelInstances/___VARIABLE_relativePath___/___FILEBASENAME___.hxx"


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP) */
