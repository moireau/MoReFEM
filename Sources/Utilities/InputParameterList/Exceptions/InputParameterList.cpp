///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 15:14:32 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <sstream>
#include <cassert>
#include <list>
#include <algorithm>

#include "Utilities/InputParameterList/Exceptions/InputParameterList.hpp"



namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string DuplicateInInputFileMsg(const std::string& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list);


    std::string UnboundInputParameterMsg(const std::string& filename,
                                         const std::string& section,
                                         const std::string& variable);


    std::string DuplicateInTupleMsg(const std::string& key);


    std::string MpiNotInitializedMsg();


    std::string FolderDoesntExistMsg(const std::string& folder);



} // namespace anonymous


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            namespace ExceptionNS
            {


                Exception::~Exception() = default;


                Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(msg, invoking_file, invoking_line)
                { }


                DuplicateInInputFile::~DuplicateInInputFile() = default;


                DuplicateInInputFile::DuplicateInInputFile(const std::string& filename,
                                                           const std::string& section,
                                                           const std::vector<std::string>& variable_list,
                                                           const char* invoking_file, int invoking_line)
                : Exception(DuplicateInInputFileMsg(filename, section, variable_list), invoking_file, invoking_line)
                { }


                UnboundInputParameter::~UnboundInputParameter() = default;

                
                UnboundInputParameter::UnboundInputParameter(const std::string& filename,
                                                             const std::string& section,
                                                             const std::string& variable,
                                                             const char* invoking_file, int invoking_line)
                : Exception(UnboundInputParameterMsg(filename, section, variable), invoking_file, invoking_line)
                { }


                DuplicateInTuple::~DuplicateInTuple() = default;


                DuplicateInTuple::DuplicateInTuple(const std::string& key,
                                                   const char* invoking_file, int invoking_line)
                : Exception(DuplicateInTupleMsg(key), invoking_file, invoking_line)
                { }



                MpiNotInitialized::~MpiNotInitialized() = default;


                MpiNotInitialized::MpiNotInitialized(const char* invoking_file, int invoking_line)
                : Exception(MpiNotInitializedMsg(), invoking_file, invoking_line)
                { }


                FolderDoesntExist::~FolderDoesntExist() = default;


                FolderDoesntExist::FolderDoesntExist(const std::string& folder, const char* invoking_file, int invoking_line)
                : Exception(FolderDoesntExistMsg(folder), invoking_file, invoking_line)
                { }

                
                
            } // namespace ExceptionNS
            
            
        } // namespace InputParameterListNS
        
        
    } // namespace Utilities
    
    
} // namespace MoReFEM




namespace // anonymous
{

    
    // Definitions of functions defined at the beginning of the file
    
    std::string DuplicateInInputFileMsg(const std::string& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list)
    {
        std::list<std::string> buf(variable_list.cbegin(), variable_list.cend());
        buf.sort();
        buf.unique();

        auto it_duplicate = buf.cbegin();

        // Find the duplicate.
        {
            std::list<std::string>::difference_type Ninstance = 0;

            for (auto end = buf.cend(); it_duplicate != end; ++it_duplicate)
            {
                Ninstance = std::count(variable_list.cbegin(), variable_list.cend(), *it_duplicate);

                if (Ninstance >= 2)
                    break;
            }
            assert(it_duplicate != buf.cend());
        }


        std::ostringstream oconv;
        oconv << "Error in input file " << filename << ": "
        "at least one duplicate found ('" << *it_duplicate << "')";

        if (!section.empty())
            oconv << " in section '" << section << '\'';
        else
            oconv << "outside of any section";

        oconv << '.';

        return oconv.str();
    }


    std::string UnboundInputParameterMsg(const std::string& filename,
                                         const std::string& section,
                                         const std::string& variable)
    {

        std::ostringstream oconv;
        oconv << "In the input file ' " << filename << "' there was a variable '" << variable << '\'';

        if (section.empty())
            oconv << " outside any section";
        else
            oconv << " in the section \'" << section << '\'';

        oconv << "; no tuple element claims this one. It means this input parameter is "
        "completely useless; please remove it from the input file.";

        return oconv.str();
    }


    std::string DuplicateInTupleMsg(const std::string& key)
    {        
        std::ostringstream oconv;
        oconv << "The key " << key << " has been found at least twice in the input parameter list; "
                "the tuple that built it is therefore ill-formed.\n";
        oconv << "It might be either the same entry is present twice in the tuple, or two entries share "
                "the same couple Section() / NameInFile()";
        
        return oconv.str();
    }



    std::string MpiNotInitializedMsg()
    {
        return "MPI was not initialized properly!";
    }


    std::string FolderDoesntExistMsg(const std::string& folder)
    {
        return "Folder " + folder + " doesn't exist whereas is was expected to.";
    }




} // namespace anonymous


/// @} // addtogroup UtilitiesGroup
