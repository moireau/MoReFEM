///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Feb 2015 10:39:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HPP_

# include <memory>
# include <vector>

# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/MatrixOrVector.hpp"

# include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================




    namespace InterpolationOperatorNS
    {


        /*!
         * \brief This operator allows to go back-and-forth between a complete vector and its state (i.e. a smaller
         * vector from which all dofs related to an essential boundary condition have been removed).
         *
         * This class is required to perform data assimilation, for which state might be known through
         * observations on only a part of the mesh.
         */
        class State
        {

        public:

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const State>;

        private:

            //! Comparison relationship used for the dofs.
            using dof_ordering_comp = Utilities::PointerComparison::Less<Dof::shared_ptr>;

            //! Type of the storage of Dirichlet dof list.
            using DirichletDofListType = Utilities::PointerComparison::Map<Dof::shared_ptr, unsigned int, dof_ordering_comp>;


        public:

            /// \name Special members.
            ///@{

            /*!
             * \class doxygen_hide_state_operator_contructor_arg
             *
             * \param[in] numbering_subset \a NumberingSubset upon which the boundary conditions are defined.
             * \param[out] target_vector_pattern Vector which structure is the one expected for the state. The target
             * vector of \a ToState() is expected to respect the pattern (in most of the case you should probably give
             * here the vector you will use later with \a ToState(). Currently this vector does not include ghosts.
             *
             * \internal Comment was added much after class implementation and there is no case in which it is applied;
             * it is likely interface could be slightly better here.
             */

            /*!
             * \brief Constructor when complete vector is defined over the whole \a god_of_dof.
             *
             * \copydoc doxygen_hide_state_operator_contructor_arg
             * \param[in] god_of_dof God of dof that includes the vector which state will be required. This
             * object is also the one aware of which dofs are bound to an essential boundary condition.
             */
            explicit State(const GodOfDof& god_of_dof,
                           const NumberingSubset& numbering_subset,
                           GlobalVector& target_vector_pattern);

            /*!
             * \brief Constructor when complete vector is defined over a finite element space of \a god_of_dof.
             *
             * Typically, the finite element space may be the area in which observations are available (in the case
             * observation and model are defined on the same mesh).
             *
             * \param[in] felt_space Finite element space to which the vector which state are required must belong to.
             * \copydoc doxygen_hide_state_operator_contructor_arg
             */
            explicit State(FEltSpace& felt_space,
                           const NumberingSubset& numbering_subset,
                           GlobalVector& target_vector_pattern);

            //! Destructor.
            ~State() = default;

            //! Copy constructor.
            State(const State&) = delete;

            //! Move constructor.
            State(State&&) = delete;

            //! Copy affectation.
            State& operator=(const State&) = delete;

            //! Move affectation.
            State& operator=(State&&) = delete;

            ///@}

            /*!
             * \brief Yields the state vector from the complete ones.
             *
             * Dofs that are on a Dirichlet boundary conditions are simply dropped.
             *
             * \param[in] vector_with_dirichlet_dof Vector including the dofs related to an essential boundary condition.
             * \param[out] vector_without_dirichlet_dof Smaller vector which Dirichlet dofs have been removed.
             * Ghosts are not handled in this resulting vector, even if they were in \a vector_with_dirichlet_dof.
             */
            void ToState(const GlobalVector& vector_with_dirichlet_dof,
                         GlobalVector& vector_without_dirichlet_dof) const;


            /*!
             * \brief Yields the complete vector from the state.
             *
             * Dirichlet dofs are padded with 0.
             *
             * \param[in] vector_without_dirichlet_dof State vector.
             * \param[out] vector_with_dirichlet_dof Bigger vector into which Dirichlet dofs have been reintroduced.
             * Ghosts are updated.
             */

            void FromState(const GlobalVector& vector_without_dirichlet_dof,
                           GlobalVector& vector_with_dirichlet_dof) const;


        private:

            /*!
             * \brief Method that does the bulk of construction.
             *
             * \param[in] god_of_dof God of dof that includes the vector which state will be required. This
             * object is also the one aware of which dofs are bound to an essential boundary condition.
             * \param[in] processor_wise_dof_list List of processor-wise dofs in the space considered (ghost excluded).
             * \copydoc doxygen_hide_state_operator_contructor_arg
             *
             */
            void Construct(const GodOfDof& god_of_dof,
                           const NumberingSubset& numbering_subset,
                           const Dof::vector_shared_ptr& processor_wise_dof_list,
                           GlobalVector& target_vector_pattern);


            //! Whether there are processor-wise Dirichlet dofs or not.
            bool AreProcessorWiseDirichletDof() const;

            #ifndef NDEBUG
            //! Number of program-wise dofs expected in the source vector.
            unsigned int NexpectedProgramWiseDofIncludingDirichlet() const noexcept;

            //! Number of program-wise dofs expected in the target vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int NexpectedProgramWiseDofExcludingDirichlet() const noexcept;

            //! Number of processor-wise dofs expected in the source vector.
            unsigned int NexpectedProcessorWiseDofIncludingDirichlet() const noexcept;

            //! Number of processor-wise dofs expected in the target vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int NexpectedProcessorWiseDofExcludingDirichlet() const noexcept;
            #endif // NDEBUG


            //! Access to the list of processor-wise (ghost excluded_ dofs related to a Dirichlet boundary condition.
            const DirichletDofListType & GetProcessorWiseDirichletDofList() const;

            //! Set the list of processor-wise (ghost excluded_ dofs related to a Dirichlet boundary condition.
            void SetProcessorWiseDirichletDofList(DirichletDofListType && processor_wise_dirichlet_dof_list);


        private:


            /*!
             * \brief List of processor-wise (ghost excluded) dofs related to a Dirichlet boundary condition.
             *
             * Key is the position of the dof in the matrix (which is not necessarily one of the dof index:
             * dof indexes match the position in the matrix only for thw whole god of dof).
             */
            DirichletDofListType processor_wise_dirichlet_dof_list_;

            #ifndef NDEBUG

            //! Number of program-wise dofs expected in the target vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int Nexpected_program_wise_dof_excluding_dirichlet_ = NumericNS::UninitializedIndex<unsigned int>();

            //! Number of processor-wise dofs expected in the target vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int Nexpected_processor_wise_dof_excluding_dirichlet_ = NumericNS::UninitializedIndex<unsigned int>();

            //! Number of program-wise dofs expected in the source vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int Nexpected_program_wise_dof_including_dirichlet_ = NumericNS::UninitializedIndex<unsigned int>();

            //! Number of processor-wise dofs expected in the source vector (i.e. once the Dirichlet dofs have been removed).
            unsigned int Nexpected_processor_wise_dof_including_dirichlet_ = NumericNS::UninitializedIndex<unsigned int>();

            #endif // NDEBUG

        };


    } // namespace InterpolationOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/StateOperator/State.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HPP_
