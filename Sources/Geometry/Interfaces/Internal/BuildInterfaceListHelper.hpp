///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Oct 2014 11:45:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_

# include <memory>
# include <vector>

# include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
# include "Geometry/Interfaces/Instances/OrientedFace.hpp"
# include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"
# include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {



            /*!
             * \brief Dispatch structure that creates the list of \a Edge if it is relevant for \a TopologyT.
             *
             * \tparam TopologyT Topology currently considered.
             * \tparam IsEdge Whether edges are relevant for the topology.
             */
            template<class TopologyT, bool IsEdge>
            struct BuildEdgeListHelper;


            /*!
             * \brief Dispatch structure that creates the list of \a Face if it is relevant for \a TopologyT.
             *
             * \tparam TopologyT Topology currently considered.
             * \tparam IsFace Whether faces are relevant for the topology.
             */
            template<class TopologyT, bool IsFace>
            struct BuildFaceListHelper;


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================

            template<class TopologyT>
            struct BuildEdgeListHelper<TopologyT, false>
            {

                static OrientedEdge::vector_shared_ptr Perform(const GeometricElt* geom_elt_ptr,
                                                               const Coords::vector_raw_ptr& coords_list,
                                                               Edge::InterfaceMap& existing_list);

            };


            template<class TopologyT>
            struct BuildEdgeListHelper<TopologyT, true>
            {

                static OrientedEdge::vector_shared_ptr Perform(const GeometricElt* geom_elt_ptr,
                                                               const Coords::vector_raw_ptr& coords_list,
                                                               Edge::InterfaceMap& existing_list);


            };


            template<class TopologyT>
            struct BuildFaceListHelper<TopologyT, false>
            {

                static OrientedFace::vector_shared_ptr Perform(const GeometricElt* geom_elt_ptr,
                                                               const Coords::vector_raw_ptr& coords_list,
                                                               Face::InterfaceMap& existing_list);

            };


            template<class TopologyT>
            struct BuildFaceListHelper<TopologyT, true>
            {

                static OrientedFace::vector_shared_ptr Perform(const GeometricElt* geom_elt_ptr,
                                                               const Coords::vector_raw_ptr& coords_list,
                                                               Face::InterfaceMap& existing_list);


            };

            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================

        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_
