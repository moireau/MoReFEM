///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#include <set>
#include <algorithm>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace VarfNS
        {
            
            
            const GlobalMatrix& GlobalMatrixStorage::GetMatrix(const NumberingSubset& row_numbering_subset,
                                                               const NumberingSubset& col_numbering_subset) const
            
            {
                const auto& storage = GetStorage();
                
                assert(!storage.empty() && "You probably forgot to call Init() method just after the constructor of "
                       "your variational formulation!");
                
                const auto it = std::find_if(storage.cbegin(),
                                             storage.cend(),
                                             NumberingSubsetNS::FindIfConditionForPair<GlobalMatrix::unique_ptr>(row_numbering_subset,
                                                                                                           col_numbering_subset));
                
                assert(it != storage.cend());
                assert(!(!*it));
                return *(*it);
            }
            
            
            GlobalMatrix& GlobalMatrixStorage::NewMatrix(const NumberingSubset& row_numbering_subset,
                                                         const NumberingSubset& col_numbering_subset)
            {
                auto&& new_item =
                std::make_unique<GlobalMatrix>(row_numbering_subset,
                                               col_numbering_subset);
                
                auto& storage = GetNonCstStorage();
                storage.emplace_back(std::move(new_item));
                
                # ifndef NDEBUG
                AssertNoDuplicate();
                # endif // NDEBUG
                
                return *(storage.back());
            }
            
            
            # ifndef NDEBUG
            void GlobalMatrixStorage::AssertNoDuplicate() const
            {
                const auto& storage = GetStorage();
                
                std::set<std::pair<unsigned int, unsigned int> > id_list;
                for (const auto& ptr : storage)
                {
                    assert(!(!ptr));
                    auto check = id_list.insert({
                        ptr->GetRowNumberingSubset().GetUniqueId(),
                        ptr->GetColNumberingSubset().GetUniqueId()
                    });
                    
                    assert(check.second && "A given unique Id should be present only once!");
                }
                
                
                
            }
            # endif // NDEBUG
            
            
        } // namespace VarfNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup
