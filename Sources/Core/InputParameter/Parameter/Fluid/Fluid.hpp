///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FLUID_x_FLUID_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FLUID_x_FLUID_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"
# include "Core/InputParameter/Parameter/MaterialProperty/VolumicMass.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section
        struct Fluid : public Crtp::Section<Fluid, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = Fluid;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Convenient alias.
            using VolumicMass = MaterialProperty::VolumicMass<self>;

            //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
            struct Viscosity : public Crtp::Section<Viscosity, Fluid>
            {



                //! Convenient alias.
                using self = Viscosity;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Fluid>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines viscosity value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };



                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;


            }; // struct Viscosity


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Density : public Crtp::Section<Density, Fluid>
            {



                //! Convenient alias.
                using self = Density;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Fluid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar
                : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines viscosity value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction
                : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };



                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId
                : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue
                : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section. // \todo  Should be private with accessor.
                section_content_type section_content_;


            }; // struct Density



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                VolumicMass,
                Density,
                Viscosity
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;



        }; // struct Fluid


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FLUID_x_FLUID_HPP_
