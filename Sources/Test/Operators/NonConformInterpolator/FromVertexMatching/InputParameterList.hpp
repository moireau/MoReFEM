/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"



namespace MoReFEM
{


    namespace TestVertexMatchingNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            fluid = 1,
            solid = 2
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            fluid = 10,
            solid = 20
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            fluid = 10,

            solid = 20
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            unknown = 1,

            chaos = 3 // this unknown is just there to ensure the dof repartition is not the same for unknown and pressure.
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex : unsigned int
        {
            unknown_on_fluid = 10,

            unknown_on_solid = 20,
            chaos = 22,
        };


        //! Indexes for the vertex matching interpolator.
        enum class InitVertexMatchingInterpolator
        {
            unknown_on_fluid = 10,
            unknown_on_solid = 20,
        };




        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::fluid)>,
            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::solid)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::fluid)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::solid)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::InterpolationFile,

            InputParameter::InitVertexMatchingInterpolator<EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_solid)>,
            InputParameter::InitVertexMatchingInterpolator<EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_fluid)>,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestVertexMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_PARAMETER_LIST_HPP_
