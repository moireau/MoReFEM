add_executable(MoReFEMTestMpiFunctions
               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              )
          
target_link_libraries(MoReFEMTestMpiFunctions
                      ${MOREFEM_MODEL})
                      
morefem_install(MoReFEMTestMpiFunctions)