///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 Apr 2016 22:51:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ALIAS_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ALIAS_HPP_


# include <functional>


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        /*!
         * \brief Convenient alias used to define the type of a shape functions.
         */
        using ShapeFunctionType = std::function<double(const LocalCoords&)>;


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ALIAS_HPP_
