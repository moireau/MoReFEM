///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 9 Oct 2014 17:00:19 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief This Crtp is used for both Edge and Face interface.
         *
         * \tparam DerivedT Either 'OrientedEdge' or 'OrientedFace'.
         * \tparam UnorientedInterfaceT Respectively 'Edge' and Face' for 'OrientedEdge' and 'OrientedFace'.
         */
        template
        <
            class DerivedT,
            class UnorientedInterfaceT
        >
        class Orientation
        {

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] interface The underlying interface without any orientation consideration.
             * \param[in] orientation Integer that tag which orientation is actually used. It is numbered from 0:
             * for instance an OrientedEdge might get 0 or 1 (for faces it depends on the nature of the underlying
             * \a GeometricElt).
             */
            explicit Orientation(const typename UnorientedInterfaceT::shared_ptr& interface,
                                 unsigned int orientation);


        protected:

            //! Destructor.
            ~Orientation() = default;

            //! Copy constructor.
            Orientation(const Orientation&) = delete;

            //! Move constructor.
            Orientation(Orientation&&) = delete;

            //! Affectation.
            Orientation& operator=(const Orientation&) = delete;

            //! Move affectation.
            Orientation& operator=(Orientation&&) = delete;


            ///@}


        public:

            //! Return the underlying Interface object (without orientation).
            const UnorientedInterfaceT& GetUnorientedInterface() const noexcept;

            //! Return the underlying Interface object (without orientation).
            UnorientedInterfaceT& GetNonCstUnorientedInterface() noexcept;

            //! Return the underlying Interface object as a smart pointer (without orientation).
            typename UnorientedInterfaceT::shared_ptr GetUnorientedInterfacePtr() const noexcept;

            /*!
             * \brief Return the list of \a Coords that delimit the interface.
             *
             * \internal <b><tt>[internal]</tt></b> Orientation unused here; accessible for metaprogramming purposes.
             *
             * \return List of \a Coords that delimit the interface.
             */
            const Coords::vector_raw_ptr& GetVertexCoordsList() const noexcept;


            //! Return the orientation.
            unsigned int GetOrientation() const noexcept;

            //! Get the identifier associated to the unoriented interface.
            unsigned int GetIndex() const noexcept;

            //! Nature of the Interface as a static method.
            constexpr static InterfaceNS::Nature StaticNature() noexcept;

            /*!
             * \brief Print the underlying coords list and the orientation.
             *
             * \param[in,out] stream Stream to which the Orientation<DerivedT, UnorientedInterfaceT> is written.
             */
            void Print(std::ostream& stream) const;


        private:

            //! Underlying unoriented object.
            typename UnorientedInterfaceT::shared_ptr unoriented_interface_;

            //! Orientation.
            const unsigned int orientation_;


        };


        /*!
         * \copydoc doxygen_hide_operator_less
         *
         * The convention is that the ordering is the same as the one of underlying Interface; in case of equality orientation
         * is also considered.
         */
        template<class DerivedT, class UnorientedInterfaceT>
        bool operator<(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                       const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept;


        /*!
         * \copydoc doxygen_hide_operator_equal
         *
         * Two Orientation<DerivedT, UnorientedInterfaceT> objects are equal if they share the same underlying Interface and the same orientation.
         */
        template<class DerivedT, class UnorientedInterfaceT>
        bool operator==(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                        const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept;




    } //  namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Internal/Orientation.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_
