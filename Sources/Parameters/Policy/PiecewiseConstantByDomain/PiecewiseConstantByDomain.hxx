///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 7 Jul 2015 17:54:00 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {

            template<ParameterNS::Type TypeT>
            PiecewiseConstantByDomain<TypeT>
            ::PiecewiseConstantByDomain(const Domain& domain,
                                        const std::vector<unsigned int>& key,
                                        const std::vector<std::decay_t<return_type>>& value)
            {
                auto& stored_values_per_domain = GetNonCstStoredValuesPerDomain();

                stored_values_per_domain.max_load_factor(Utilities::DefaultMaxLoadFactor());

                const unsigned int key_size = static_cast<unsigned int>(key.size());
                stored_values_per_domain.reserve(key_size);

                auto& mapping_geom_elt_domain = GetNonCstMapGeomEltDomain();
                mapping_geom_elt_domain.max_load_factor(Utilities::DefaultMaxLoadFactor());

                assert(key.size() == value.size() && "Should have been checked before at the creation "
                       "of the input_parameter_file.");

                for (unsigned int i = 0 ; i < key_size ; ++i)
                {
                    auto check = stored_values_per_domain.insert({key[i], value[i]});

                    if (!(check.second))
                    {
                        throw Exception("Two domains with the same key have been given in "
                                        "PiecewiseConstantByDomain parameter.",
                                        __FILE__, __LINE__);
                    }
                }

                decltype(auto) geometric_mesh_region = domain.GetGeometricMeshRegion();

                const auto& geom_elem_list = geometric_mesh_region.GetGeometricEltList();

                const auto& domain_manager_instance = DomainManager::GetInstance();

                for (const auto& geom_elt_ptr : geom_elem_list)
                {
                    for (const auto& pair : stored_values_per_domain)
                    {
                        // Domain here is expected to be a subset of the whole \a Domain upon which the \a Parameter is
                        // built.
                        const auto& current_domain = domain_manager_instance.GetDomain(pair.first, __FILE__, __LINE__);

                        assert(!(!geom_elt_ptr));
                        const auto& geom_elt = *geom_elt_ptr;

                        if (current_domain.IsGeometricEltInside(geom_elt))
                        {
                            auto check = mapping_geom_elt_domain.insert({geom_elt.GetIndex(), current_domain.GetUniqueId()});

                            if (!(check.second))
                            {
                                throw Exception("A geometric element is in two domains with two different values for "
                                                "the PiecewiseConstantByDomain parameter.",
                                                __FILE__, __LINE__);
                            }
                        }
                    }
                }
            }


            template<ParameterNS::Type TypeT>
            inline typename PiecewiseConstantByDomain<TypeT>::return_type PiecewiseConstantByDomain<TypeT>
            ::GetValueFromPolicy(const local_coords_type& local_coords,
                                 const GeometricElt& geom_elt) const
            {
                static_cast<void>(local_coords);

                // First identify the relevant domain for the current \a geom_elt.
                const unsigned int geom_elt_index = geom_elt.GetIndex();

                const auto& mapping_geom_elt_domain = GetMapGeomEltDomain();

                const auto it_domain = mapping_geom_elt_domain.find(geom_elt_index);

                assert(it_domain != mapping_geom_elt_domain.cend());

                const unsigned int domain_unique_id = it_domain->second;

                // Then fetch the value related to this domain.
                const auto& stored_values_per_domain = GetStoredValuesPerDomain();

                const auto it = stored_values_per_domain.find(domain_unique_id);

                assert(it != stored_values_per_domain.cend());

                return it->second;
            }


            template<ParameterNS::Type TypeT>
            inline typename PiecewiseConstantByDomain<TypeT>::return_type PiecewiseConstantByDomain<TypeT>
            ::GetAnyValueFromPolicy() const
            {
                const auto& stored_values_per_domain = GetStoredValuesPerDomain();
                assert(!stored_values_per_domain.empty());
                return stored_values_per_domain.cbegin()->second;
            }


            template<ParameterNS::Type TypeT>
            [[noreturn]] typename PiecewiseConstantByDomain<TypeT>::return_type
            PiecewiseConstantByDomain<TypeT>::GetConstantValueFromPolicy() const noexcept
            {
                assert(false && "Parameter class should have guided toward GetValue()!");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT>
            inline constexpr bool PiecewiseConstantByDomain<TypeT>::IsConstant() const noexcept
            {
                return false;
            }


            template<ParameterNS::Type TypeT>
            void PiecewiseConstantByDomain<TypeT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Domain index; Value by domain:" << std::endl;

                const auto& stored_values_per_domain = GetStoredValuesPerDomain();

                for (const auto& pair : stored_values_per_domain)
                    out << pair.first << ';' << pair.second << std::endl;
            }


            template<ParameterNS::Type TypeT>
            inline const typename PiecewiseConstantByDomain<TypeT>::storage_type&
            PiecewiseConstantByDomain<TypeT>::GetStoredValuesPerDomain() const noexcept
            {
                return stored_values_per_domain_;
            }


            template<ParameterNS::Type TypeT>
            inline typename PiecewiseConstantByDomain<TypeT>::storage_type&
            PiecewiseConstantByDomain<TypeT>::GetNonCstStoredValuesPerDomain() noexcept
            {
                return const_cast<storage_type&>(GetStoredValuesPerDomain());
            }


            template<ParameterNS::Type TypeT>
            inline const std::unordered_map<unsigned int, unsigned int>& PiecewiseConstantByDomain<TypeT>
            ::GetMapGeomEltDomain() const noexcept
            {
                return mapping_geom_elt_domain_;
            }


            template<ParameterNS::Type TypeT>
            inline std::unordered_map<unsigned int, unsigned int>& PiecewiseConstantByDomain<TypeT>
            ::GetNonCstMapGeomEltDomain() noexcept
            {
                return const_cast<std::unordered_map<unsigned int, unsigned int>&>(GetMapGeomEltDomain());
            }

            template<ParameterNS::Type TypeT>
            inline void PiecewiseConstantByDomain<TypeT>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false);
                exit(EXIT_FAILURE);
            }


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_
