///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_OPS_TYPE_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_OPS_TYPE_HPP_


# include <vector>


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Traits
            {


                namespace Impl
                {


                    /*!
                     * \brief  This helper class is used to handle integral types that are not supported by Ops.
                     *
                     * \tparam T Type being checked. It should be a POD type.
                     * \tparam IsNonOpsIntegralT True if \a T is an integral type not supported directly by Ops.
                     */
                    template<class T, bool IsNonOpsIntegralT>
                    struct ops_typeHelper2;


                    //! \cond IGNORE_BLOCK_IN_DOXYGEN
                    template<class T>
                    struct ops_typeHelper2<T, true>
                    {
                        using type = int;
                    };


                    template<class T>
                    struct ops_typeHelper2<T, false>
                    {
                        using type = T;
                    };
                    //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                    /*!
                     * \brief Determine whether there is a non-ops integral type involved.
                     *
                     * This can be directly or within a std::vector; in the latter case the test is upon the type
                     * encapsulated in the vector.
                     *
                     * See ops_type comment for more details.
                     */
                    template<class ReturnTypeT>
                    struct ops_typeHelper
                    {
                        //! Whether \a ReturnTypeT is an integral type.
                        static constexpr bool is_non_ops_integral =
                        (std::is_integral<ReturnTypeT>::value
                         && !std::is_same<ReturnTypeT, int>::value
                         && !std::is_same<ReturnTypeT, bool>::value);

                        //! Yields a type compatible with Ops.
                        using type = typename ops_typeHelper2<ReturnTypeT, is_non_ops_integral>::type;
                    };

                    //! Case in which ReturnTypeT is a std::vector.
                    template<class T>
                    struct ops_typeHelper<std::vector<T>>
                    {
                    private:

                        //! True if \a T is an integral type not handled directly by Ops.
                        static constexpr bool is_non_ops_integral = ops_typeHelper<T>::is_non_ops_integral;

                        //! If is_non_ops_integral is true, \a EncapsulatedType is an int. Otherwise it is \a T.
                        using EncapsulatedType = typename ops_typeHelper2<T, is_non_ops_integral>::type;

                    public:

                        //! Type if a vector of EncapsulatedType.
                        using type = typename std::vector<EncapsulatedType>;

                    };



                    /*!
                     * \brief Choose a Ops-compatible type.
                     *
                     * Ops does handle only a handful of types; peculiarly only int and bool are recognized as integral constants.
                     * However, some of our input parameters are for instance unsigned int...
                     *
                     * So in this case it is converted into a regular ops_type (namely an int) and a static_cast to the proper type
                     * is performed later.
                     *
                     * Likewise if stored in a std::vector.
                     */
                    template<class T>
                    struct ops_type
                    {
                        //! Figures out the Ops-compatible type related to \a T.
                        using type = typename ops_typeHelper<T>::type;
                    };



                } // namespace Impl


            } // namespace Traits


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_OPS_TYPE_HPP_
