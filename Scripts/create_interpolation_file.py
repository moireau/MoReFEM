import os

def ReadVertexList(filename):
    """Read crudely the vertice part of a Medit mesh."""
    vertex_list = []

    FILE = open(filename)
    
    is_reading_vertice_list = False
    just_read_keyword = False

    for line in FILE:

        line = line.strip()
        if line == "Vertices":
            is_reading_vertice_list = True
            just_read_keyword = True
            continue
            
        if not is_reading_vertice_list:
            continue
            
        if just_read_keyword:
            Nvertex = int(line)
            just_read_keyword = False
            continue
                        
        splitted = line.split()
    
        if len(splitted) != 3:
            break
            
        splitted = splitted[:-1]
        
        values = [float(x) for x in splitted]    
        vertex_list.append(values)
        
    return vertex_list


def CreateInterpolationFile(mesh1, mesh2, out):
    """Quick-and-dirty creation of the file matching vertices of two meshes, that is determined through the look up of float coordinates."""
    
    vertex_list_1 = ReadVertexList(mesh1)
    vertex_list_2 = ReadVertexList(mesh2)    
    
    Nvertex_1 = len(vertex_list_1)
    
    FILE_out = open(out, 'w')
    
    FILE_out.write("# Vertex index in mesh 1 / vertex index in mesh 2\n")
    
    print  vertex_list_1
    
    for vertex_index_1 in range(0, Nvertex_1):
        vertex = vertex_list_1[vertex_index_1]

        position_list = [1 + i for i,x in enumerate(vertex_list_2) if x == vertex]
        assert(len(position_list) < 2)
        if position_list:
            FILE_out.write("{1} {0}\n".format(vertex_index_1 + 1, position_list[0]))
    
    

if __name__ == "__main__":
    CreateInterpolationFile("{0}/Codes/MoReFEM/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh".format(os.environ["HOME"]),
                            "{0}/Codes/MoReFEM/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh".format(os.environ["HOME"]),
                            "{0}/Codes/MoReFEM/Data/Interpolation/TestVertexMatching.hhdata".format(os.environ["HOME"]))
                            

    