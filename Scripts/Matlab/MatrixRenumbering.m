function Mat_out = MatrixRenumbering(Mat_in, indices_out)

nDof = length(indices_out);

Mat_out = zeros(nDof, nDof);

for i=1:nDof
    for j=1:nDof
        Mat_out(indices_out(i) + 1, indices_out(j) + 1) = Mat_in(i,j);
    end
end

end